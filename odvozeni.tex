\section{Odvození}

V~této kapitole odvodím rovnice pro dostředivé zrychlení.
Nejprve provedu základní odvození s~pomocí zjednodušení oblouku.
Následně stejnou situaci vyřeším pomocí vektorů.
Odtud budu postupně uvolňovat předpoklady postupů, až se dostanu k~obecnému pohybu.

%   ___  _     _             _
%  / _ \| |__ | | ___  _   _| | ___   _
% | | | | '_ \| |/ _ \| | | | |/ / | | |
% | |_| | |_) | | (_) | |_| |   <| |_| |
%  \___/|_.__/|_|\___/ \__,_|_|\_\\__, |
%                                 |___/

\label{sec:obvodove_odvozeni}
\subsection{Obvodová rychlost a zrychlení}
Nejdříve odvodím velikost obvodové rychlosti pro pohyb po kružnici. Vyjdu přitom z~vlastností obloukové míry.

Oblouková míra je definovaná tak, že velikost úhlu zároveň určuje délku oblouku, který úhel vytíná na jednotkové kružnici.
Ekvivalentem úhlu \ang{360} je totiž hodnota \(2\pi\) -- obvod jednotkové kružnice.
Ta je znázorněná vlevo na obrázku \ref{fig:oblouky:prvni}.
Přes přímou úměrnost pak můžeme počítat i s~jinými úhly, než je \(2\pi\).
\begin{align*}
    o~&= 2\pi r \\
    r_1 &= 1      \\
    o_1 &= 2\pi   \\
    \frac{s_1}{o_1} &= \frac{\varphi}{2\pi} \\
    s_1 &= \varphi
\end{align*}

Pomocí podobnosti pak můžeme pronést následující tvrzení.
Změní-li se poloměr kružnice z~\(1\) na \(r\), pak se délka oblouku vyseknutého úhlem \(\varphi\) změní z~\(\varphi\) na \(\varphi\cdot r\) jednotek.
Příklad takového oblouku je znázorněný vpravo na obrázku \ref{fig:oblouky:prvni}.
\begin{align}
    s~&= \varphi \cdot r \label{eqn:zaklady:delka_oblouku}
\end{align}

\begin{figure}[h]\centering{}
    \input{obrazky/jednotkova-kruznice} \caption{Obvod jednotkové kružnice a výseč obecné kružnice} \label{fig:oblouky:prvni}
\end{figure}

Pod~úhlem se však mohou skrývat ještě další jednotky.
Například rychlost udává, jakou vzdálenost (či úhel, v~případě úhlové rychlosti) objekt urazí za jednotku času.
Předchozí úvahu tedy můžu rozšířit i na lineární a úhlovou rychlost, případně na lineární a úhlové zrychlení.
Matematicky úpravu lze vyjádřit tak, že obě strany rovnice vydělíme časovým intervalem, za který změna proběhla.
\begin{align*}
    \frac{\Delta s}{\Delta t} &= \frac{\Delta \varphi \cdot r}{\Delta t} \\
    v_p &= \omega_p \cdot r \\
    \frac{\Delta v}{\Delta t} &= \frac{\Delta \omega \cdot r}{\Delta t} \\
    a_p &= \varepsilon_p \cdot r
\end{align*}
Obdobně můžu obě strany zderivovat, poté nebudou ve výrazu figurovat průměrné, ale okamžité hodnoty veličin.
\begin{align}
    \frac{d s}{d t} &= \frac{d \varphi}{d t} \cdot r \nonumber \\
    v~&= \omega \cdot r \label{eqn:zaklady:obvodova_rychlost} \\
    \frac{d v}{d t} &= \frac{d \omega}{d t} \cdot r \nonumber \\
    a~&= \varepsilon \cdot r \label{eqn:zaklady:obvodove_zrychleni}
\end{align}

Dokázali jsme tedy tři základní vztahy: pro délku oblouku na kružnici \(s=\varphi r\), pro obvodovou rychlost \(v=\omega r\) a pro obvodové zrychlení \(a=\varepsilon r\).


%  _   _          _           _                                _                         _
% | | | | ___ ___| |__  _ __ (_) ___ _____   _____    ___   __| |_   _____ _______ _ __ (_)
% | | | |/ __/ _ \ '_ \| '_ \| |/ __/ _ \ \ / / _ \  / _ \ / _` \ \ / / _ \_  / _ \ '_ \| |
% | |_| | (_|  __/ |_) | | | | | (_| (_) \ V /  __/ | (_) | (_| |\ V / (_) / /  __/ | | | |
%  \___/ \___\___|_.__/|_| |_|_|\___\___/ \_/ \___|  \___/ \__,_| \_/ \___/___\___|_| |_|_|
%

\label{sec:ucebnice}
\subsection{Učebnicové odvození}
Začnu s~odvozením, které se nachází v~učebnici \cite[str. 58]{ucebnice} ve výkladu o~pohybu po kružnici.
Použijeme při tom podobnost trojúhelníků a geometrické zjednodušení oblouku na úsečku.

Budeme uvažovat následující situaci. Těleso zastoupené hmotným bodem se pohybuje stálou úhlovou rychlostí po kružnici, viz obr. \ref{fig:zjednoduseni:polohy}.
V~bodech A~a B, kde se těleso nachází v~časech \(t_0\) a \(t_0+\Delta t\), jsou vyznačené příslušné vektory rychlosti.

\begin{figure}[h]\centering{}
    \input{obrazky/ucebnice-kruznice} \caption{Poloha tělesa v~bodě A~a B} \label{fig:zjednoduseni:polohy}
\end{figure}

Abychom zjistili vektor zrychlení \(\vect{a}\), přeneseme si vektory \(\vect{v_0}\) a \(\vect{v_1}\) do stejného
počátku, viz obr. \ref{fig:zjednoduseni:porovnani}. Vycházíme přitom z~toho, že pro průměrné zrychlení platí:
\[\vect{a_p} = \frac{\Delta\vect{v}}{\Delta{}t}\]

Jak se bude \(\Delta{}t\) a tím pádem \(\Delta{}\vect{v}\) zmenšovat, bude průměrné zrychlení stále bližší okamžitému zrychlení.
\[ \vect{a} = \frac{d\vect{v}}{dt} = \lim_{\Delta{}t\to0} \frac{\Delta\vect{v}}{\Delta t} \]

\begin{figure}[h]\centering{}
    \input{obrazky/ucebnice-prirovnani} \caption{Porovnání uražené vzdálenosti a rozdílu vektorů rychlosti} \label{fig:zjednoduseni:porovnani}
\end{figure}

Dráhu \(s\) uraženou tělesem po oblouku za dobu \(\Delta t\) si vyjádříme jako \(\varphi \cdot r\), viz vztah (\ref{eqn:zaklady:delka_oblouku}).
Je-li úhel \(\varphi\) na obrázku \ref{fig:zjednoduseni:porovnani} dostatečně malý, můžeme provést geometrické zjednodušení.
Oblouk \arc{AB} totiž víceméně splyne s~úsečkou AB.
Délku této úsečky si můžeme vyjádřit jako v~pravoúhlém trojúhelníku -- úhel svíraný úsečkou a průvodičem bodu se bude blížit pravému úhlu.
Matematicky to lze vyjádřit následujícím způsobem:
\[ \text{Pro } -\frac{\pi}{2} \ll |\varphi| \ll \frac{\pi}{2} \text{ platí: }\quad \sin\varphi \approx \varphi  \]

Z~obrázku pak můžeme pozorovat, že oba trojúhelníky si jsou podobné. K~důkazu použijeme větu \textit{sus}:
trojúhelníky se musí shodovat v~poměru dvou sobě si odpovídajících stran a úhlu jimi svíraném.
První polovina podmínky je splněná tím, že oba trojúhelníky jsou rovnoramenné -- poloměr \(r\) ani velikost obvodové rychlosti \(v\) se nemění.
Druhá polovina podmínky je zajištěna tím, že úhel je v~obou případech roven \(\varphi\).
\begin{equation}
    \frac{|\Delta\vect{v}|}{|\vect{v_{0}}|} = \frac{s}{r}
\end{equation}
Odtud můžeme provést matematickou úpravu takovou, abychom dostali zrychlení.
\begin{align}
    \frac{|\Delta\vect{v}|}{|\vect{v_{0}}|} &= \varphi                                        \label{eq:phik:kr1}  \\
    |\Delta\vect{v}|                        &= \varphi \cdot |\vect{v_{0}}|                   \label{eq:phik:kr2}  \\
    \frac{|\Delta\vect{v}|}{\Delta t}       &= \frac{\varphi}{\Delta t} \cdot |\vect{v_{0}}|  \label{eq:phik:kr3}  \\
    a                                       &= \omega \cdot |\vect{v_{0}}|                    \label{eq:phik:kr4}  \\
    a                                       &= \omega \cdot \omega \cdot r                    \label{eq:phik:kr5}  \\
    a                                       &= \omega^{2} \cdot r                             \label{eq:phik:kr6}
\end{align}

V~rovnici \ref{eq:phik:kr1} jsem dosadil poměr dráhy \(s\) a poloměru \(r\) zpět na úhel \(\varphi\).
V~rovnici \ref{eq:phik:kr2} jsem rychlost \(|\vect{v_{0}}|\) přemístil na druhou stranu rovnice,
abych v~kroku \ref{eq:phik:kr3} rovnici mohl vydělit časovým intervalem \(\Delta t\).
Pak si v~kroku \ref{eq:phik:kr4} vyjádřím zrychlení bodu a jeho úhlovou rychlost.
V~krocích \ref{eq:phik:kr5} a \ref{eq:phik:kr6} si dosadím obvodovou rychlost a zjednoduším.

Na závěr upravíme výsledek pomocí vztahu \(v = \omega \cdot r\) (\ref{eqn:zaklady:obvodova_rychlost}) tak, aby ve výsledku vystupovala obvodová rychlost.
\begin{align}
    a &= \frac{v^{2}}{r}
\end{align}



%  ____            _                               _       _                       _
% |  _ \  ___ _ __(_)_   ____ _  ___ ___    __ _  (_)_ __ | |_ ___  __ _ _ __ __ _| |_   _
% | | | |/ _ \ '__| \ \ / / _` |/ __/ _ \  / _` | | | '_ \| __/ _ \/ _` | '__/ _` | | | | |
% | |_| |  __/ |  | |\ V / (_| | (_|  __/ | (_| | | | | | | ||  __/ (_| | | | (_| | | |_| |
% |____/ \___|_|  |_| \_/ \__,_|\___\___|  \__,_| |_|_| |_|\__\___|\__, |_|  \__,_|_|\__, |
%                                                                  |___/             |___/

\subsection{Přechod na diferenciální a integrální počet}

Předcházející odvození můžeme vylepšit tak, aby nebylo potřeba zanedbávat velikost úhlu.
Nápad na toto odvození jsem převzal z~\cite{wikiCentripetal} a z~\cite{wikiKinematics}.

Budeme uvažovat, že hmotný bod A~se pohybuje stálou úhlovou rychlostí \(\omega = \omega_{0}\) okolo středu otáčení S.
Bod se bude pohybovat po kružnici, jeho vzdálenost \(r\) od bodu S~bude tedy také neměnná.

\subsubsection{Polohový vektor}

K~popsání pohybu využijeme průvodič (polohový vektor) \(\textbf{SA} = \vect{r}\). Situace je znázorněná na obrázku \ref{fig:int1:pruvodic}.
\begin{figure}[h]\centering{}
    \input{obrazky/derivace-pruvodic} \caption{Průvodič bodu A} \label{fig:int1:pruvodic}
\end{figure}

Nejprve jej zapíšeme pomocí polární soustavy souřadnic -- v~ní je poloha vyjádřena právě délkou průvodiče a úhlem, který průvodič svírá s~osou \textit{x}.
\begin{align}
    \vect{r} &=
        \begin{pmatrix}
            r       \\
            \varphi
        \end{pmatrix} \label{eq:int1:vektor_r}
\end{align}
Úhlovou polohu si můžeme vyjádřit ze zadané úhlové rychlosti.
Jestliže rychlost zintegrujeme podle času, dostaneme funkci úhlu, který průvodič svírá s~osou \textit{x}.
\begin{align*}
    \varphi(t) &= \int^{t}_{t_{0}} {\omega(\tau)} \,d\tau \\
    \varphi(t) &= \omega_{0} t + \varphi_{0} \numberthis \label{eq:int1:uhel_v_case}
\end{align*}
Pak si vektor \(\vect{r}\) převedeme do kartézské soustavy souřadnic. To nám umožní jej dále derivovat.
\begin{align*}
    x(t) &= r \cos(\omega_{0} t + \varphi_{0}) \\
    y(t) &= r \sin(\omega_{0} t + \varphi_{0})
\end{align*}

Ještě provedeme malou úpravu. Výsledný polohový vektor můžeme zapsat jako součin vzdálenosti bodu A~od bodu S~a
jednotkového\footnotemark vektoru určujícího směr průvodiče.
\footnotetext{Tuto vlastnost jsem dokázal v~oddíle \ref{jednotkove_vektory}.}
\begin{align*}
    \vect{r}(t) &= r \cdot{}
        \begin{pmatrix}[1.2]
            \,\cos(\omega_{0} t + \varphi_{0})\, \\
            \,\sin(\omega_{0} t + \varphi_{0})\,
        \end{pmatrix}
\end{align*}

\subsubsection{Vektor rychlosti}

Bod A~ale nezůstává na stále stejném místě, naopak se pohybuje.
K~popisu jeho pohybu budeme tedy potřebovat znát také vektor rychlosti.
Ten bude směřovat tečně na kružnici, po které se bod pohybuje -- kdyby náhle na bod přestala působit dostředivá síla, bod by pokračoval v~rovnoměrném přímočarém pohybu.
Situace je znázorněná na obrázku \ref{fig:int1:rychlost}.
\begin{figure}[h]\centering{}
    \input{obrazky/derivace-rychlost} \caption{Rychlost bodu A} \label{fig:int1:rychlost}
\end{figure}

Abych zjistil vektor rychlosti \(\vect{v}\), potřeboval bych polohový vektor určený \(r\) a \(\varphi(t)\) zderivovat. Obecně totiž platí:
\begin{align*}
    \vect{v}(t) &= \frac{d\vect{r}(t)}{dt}
\end{align*}
Funkce \(x(t)\), \(y(t)\) kartézských souřadnic bodu mohu nyní zderivovat.
Tím získám potřebný vektor rychlosti.
\begin{align*}
    x'(t) &= -\omega_{0} r \cdot \sin(\omega_{0} t + \varphi_{0}) \\
    y'(t) &= \phantom{-}\omega_{0} r \cdot \cos(\omega_{0} t + \varphi_{0})
\end{align*}
Vektor opět můžeme rozdělit na součin jeho velikosti a jednotkového směrového vektoru.
\begin{align*}
    \vect{v}(t) &= \omega_{0} r \cdot{}
        \begin{pmatrix}[1.2]
            \,         -  \sin(\omega_{0} t + \varphi_{0})\, \\
            \,\phantom{-} \cos(\omega_{0} t + \varphi_{0})\,
        \end{pmatrix}
\end{align*}
Při pohledu zpět si můžeme všimnout, že velikost vektoru se shoduje s~obvodovou rychlostí \(v\) odvozenou v~oddíle \ref{sec:obvodove_odvozeni}.
\begin{align}
    v~&= \omega_{0} r \label{eq:int1:velikost_rychlosti}
\end{align}

\subsubsection{Vektor zrychlení}

Jak jsem zmínil výše, kdyby na bod přestalo působit dostředivé zrychlení, začal by se pohybovat po přímce pryč.
Dostředivá síla tedy musí bod přitahovat ke středu otáčení a tím neustále otáčet jeho vektor rychlosti.
Situace je znázorněná na obrázku \ref{fig:int1:zrychleni}.
\begin{figure}[h]\centering{}
    \input{obrazky/derivace-zrychleni} \caption{Zrychlení bodu A} \label{fig:int1:zrychleni}
\end{figure}

Pokud zderivujeme vektor rychlosti z~předchozí části, vyjde nám podle definice vektor zrychlení.
\begin{align*}
    \vect{a}(t) &= \frac{d\vect{v}(t)}{dt}
\end{align*}
Ke zderivování použijeme rovnice souřadnic vektoru \(\vect{v}\).
Pro zrychlení nám vyjdou následující souřadnice:
\begin{align*}
    x''(t) &= -\omega_{0}^{2} r \cdot \cos(\omega_{0} t + \varphi_{0}) \\
    y''(t) &= -\omega_{0}^{2} r \cdot \sin(\omega_{0} t + \varphi_{0})
\end{align*}
Dále zbývá vektor rozložit na jeho směr a velikost.
\begin{align*}
    \vect{a}(t) &= \omega_{0}^{2} r \cdot{}
        \begin{pmatrix}[1.2]
            \,-\cos(\omega_{0} t + \varphi_{0})\, \\
            \,-\sin(\omega_{0} t + \varphi_{0})\,
        \end{pmatrix}
\end{align*}
Můžeme vidět, že velikost zrychlení \(a\) bude následující:
\begin{align*}
    a &= \omega_{0}^{2} r
\end{align*}
Při použití vztahu (\ref{eq:int1:velikost_rychlosti}) ji ale také můžeme vyjádřit pomocí obvodové rychlosti.
Opět dostáváme výsledek shodný s~předchozím odvozením, viz \ref{sec:ucebnice}.
\begin{align*}
    a &= \frac{v^{2}}{r}
\end{align*}

\label{jednotkove_vektory}
\subsubsection{Důkaz předpokladu o~délce vektorů}

Během odvození jsem použil vektory určené pomocí funkcí vyjadřujících jeho kartézské souřadnice.
Tyto funkce měly formu součinu členu společného pro obě souřadnice, znaménka navíc a jedné z~goniometrických funkcí sinus a cosinus.
Goniometrické funkce se při derivování mezi souřadnicemi prohodily, vždy ale každá funkce připadla právě jedné souřadnici.
Nyní budu uvažovat, že souřadnice x závisí na funkci cosinus.
\begin{align*}
    x &= a \cdot \pm \cos\varphi \\
    y &= a \cdot \pm \sin\varphi \\
    a &\in \reals^{+}
\end{align*}
Při úpravě na vektorový zápis jsem závislosti přepsal jako součin vektoru a skaláru \(a\).
Hodnotu \(a\) jsem přitom považoval za délku vektoru a vektor zapsaný v~závorce za jednotkový směrový vektor.
\begin{align*}
    \vect{u} &= a \cdot{}
        \begin{pmatrix}[1.2]
            \,\pm \cos\varphi\, \\
            \,\pm \sin\varphi\,
        \end{pmatrix}
\end{align*}
Pravdivost tvrzení ověříme tak, že si spočítáme délku tohoto vektoru.
Pokud je směrový vektor opravdu jednotkový, bude mít výsledný vektor velikost \(a\).
\begin{align*}
    |\vect{u}| &= \sqrt{(a \cdot \pm\cos\varphi)^{2} + (a \cdot \pm\sin\varphi)^{2}} \\
    |\vect{u}| &= a \cdot \sqrt{\cos^{2}\varphi + \sin^{2}\varphi} \numberthis\label{eq:int1:uhlove_zjednoduseni} \\
    |\vect{u}| &= a \cdot \sqrt{1} \\
    |\vect{u}| &= a
\end{align*}
Klíč leží v~rovnici (\ref{eq:int1:uhlove_zjednoduseni}).
Zde stačí použít vztah \(\sin^{2} x + \cos^{2} x = 1\), který vyplývá z~vlastností jednotkové kružnice.

Dokázal jsem tedy, že předpokládaný výrok platí.

\subsubsection{Důkaz kolmosti vektorů}

Zde bych chtěl dokázat, že vzájemné polohy vektorů pozice, rychlosti a zrychlení jsou opravdu takové, jak jsem je znázornil.
Na obrázcích \ref{fig:int1:pruvodic}, \ref{fig:int1:rychlost} a \ref{fig:int1:zrychleni} jsem vektory natočil jako na obrázku \ref{fig:int1:smery}.
Vektor rychlosti byl kolmý na polohový vektor a na vektor zrychlení -- ten ale měl opačný směr než polohový vektor.
\begin{figure}[h]\centering{}
    \input{obrazky/derivace-smery} \caption{Přenesené směrové vektory} \label{fig:int1:smery}
\end{figure}
\[ \vect{u} \bot \vect{v} \,\land\, \vect{v} \bot \vect{w} \,\land\, \vect{u} = -\vect{w} \]
Vektor \(\vect{u}\) zde představuje směr polohového vektoru \(\vect{r}\), \(\vect{v}\) směr vektoru rychlosti \(\vect{v}\) a \(\vect{w}\) směr vektoru zrychlení \(\vect{a}\).
Směrové vektory zapíšeme následujícím způsobem:
\begin{align*}
    \vect{u} &=
        \begin{pmatrix}[1.2]
            \, + \cos\varphi\, \\
            \, + \sin\varphi\,
        \end{pmatrix} \\
    \vect{v} &=
        \begin{pmatrix}[1.2]
            \, - \sin\varphi\, \\
            \, + \cos\varphi\,
        \end{pmatrix} \\
    \vect{w} &=
        \begin{pmatrix}[1.2]
            \, - \cos\varphi\, \\
            \, - \sin\varphi\,
        \end{pmatrix}
\end{align*}

První nezbytnou podmínkou je kolmost \(\vect{u}\) a \(\vect{v}\). Tu dokážeme pomocí skalárního součinu \(\vect{u} \cdot \vect{v}\), který by měl být roven nule.
\begin{align*}
    \vect{u} \cdot \vect{v} &= - \cos\varphi \cdot \sin\varphi + \sin\varphi \cdot \cos\varphi = 0
\end{align*}
Skalární součin je nulový, vektory tedy svírají pravý úhel.

Druhou podmínkou je kolmost vektoru \(\vect{v}\) na \(\vect{w}\).
\begin{align*}
    \vect{v} \cdot \vect{w} &= - \sin\varphi \cdot \cos\varphi + \cos\varphi \cdot \sin\varphi = 0
\end{align*}
Tyto dva vektory také svírají pravý úhel.

Posledním předpokladem je, že vektory \(\vect{u}\) a \(\vect{w}\) jsou opačné. Jejich součtem v~takovém případě je nulový vektor.
\begin{align*}
    \vect{u} + \vect{w} &=
        \begin{pmatrix}[1.2]
            \, \cos\varphi - \cos\varphi\, \\
            \, \sin\varphi - \sin\varphi \,
        \end{pmatrix} = \vect{0}
\end{align*}
Součet je opravdu nulový. Dokázal jsem tedy, že vektory jsou na obrázcích natočené správně.


%  ____
% |  _ \ _ __ ___  _ __ ___   ___ _ __  _ __   __ _    ___  _ __ ___   ___  __ _  __ _
% | |_) | '__/ _ \| '_ ` _ \ / _ \ '_ \| '_ \ / _` |  / _ \| '_ ` _ \ / _ \/ _` |/ _` |
% |  __/| | | (_) | | | | | |  __/ | | | | | | (_| | | (_) | | | | | |  __/ (_| | (_| |
% |_|   |_|  \___/|_| |_| |_|\___|_| |_|_| |_|\__,_|  \___/|_| |_| |_|\___|\__, |\__,_|
%                                                                          |___/

\subsection{Proměnlivá úhlová rychlost}

V~reálném životě se často stává, že rychlost otáčení nezůstává stejná, nýbrž se s~časem mění.
Jako příklad si můžeme vzít dětský kolotoč.
Ve klidovém stavu se nepohybuje.
Jakmile ale po obvodu zapůsobíme silou, kolotoč se začne otáčet daným směrem.
Změna úhlové rychlosti je způsobena právě touto tečnou silou.
Obecně tedy odvodím vztah mezi dostředivým zrychlením a proměnlivou úhlovou rychlostí.
Budu při tom vycházet z~předchozího odvození, částečně se ale budu držet zdroje \cite{wikiCentripetal}.

Abych postupoval opravdu obecně, definuji si úhlovou rychlost pouze jako funkci závislou na čase.
Stanovím jen podmínku, že funkce má určitý integrál na intervalu \(\langle t_{0};t\rangle\) a derivaci v~bodě \(t\).
\begin{align*}
    &\omega(t)\colon \langle t_0; t \rangle \rightarrow \reals{} \\
    &t, t_{0} \in \reals, t_{0} < t\colon \exists \int^{t}_{t_{0}} \omega(\tau) \,d\tau \land \exists \omega'(t)
\end{align*}
Díky těmto podmínkám mohu určit úhlové zrychlení bodu \(\varepsilon\) a úhel \(\varphi\), který průvodič bodu svírá se svojí původní polohou.
\begin{align*}
    \varepsilon(t) &= \omega'(t) \\
    \varphi(t) &= \int^{t}_{t_{0}} \omega(\tau) \,d\tau
\end{align*}


\subsubsection{Polohový vektor}

Začneme polohovým vektorem. Ten bude v~polárních souřadnicích určený stálým poloměrem \(r\) a proměnlivou úhlovou rychlostí \(\varphi(t)\).
\begin{align*}
    r(t) &= r \\
    \varphi(t) &= \varphi(t)
\end{align*}
Dále jej převedeme do kartézských souřadnic.
\begin{align*}
    x(t) &= r \cos\varphi(t) \\
    y(t) &= r \sin\varphi(t)
\end{align*}
Výsledný vektor tedy bude mít následující formu:
\begin{align*}
    \vect{r}(t) &= r \cdot{}
        \begin{pmatrix}[1.2]
            \, \cos\varphi(t) \, \\
            \, \sin\varphi(t) \,
        \end{pmatrix}
\end{align*}


\subsubsection{Vektor rychlosti}

Vektor rychlosti jsem v~předchozím odvození získal zderivováním polohového vektoru.
Zde mi v~tom taktéž nic nebrání.
Výsledné souřadnice budou následující:
\begin{align*}
    x'(t) &=          -  r \omega(t) \cdot \sin\varphi(t) \\
    y'(t) &= \phantom{-} r \omega(t) \cdot \cos\varphi(t)
\end{align*}
Vektor pak přepíšeme do obvyklého zápisu.
\begin{align*}
    \vect{v}(t) &= r \omega(t) \cdot
        \begin{pmatrix}[1.2]
            \,          -  \sin\varphi(t) \, \\
            \, \phantom{-} \cos\varphi(t) \,
        \end{pmatrix}
\end{align*}
Ze výsledku můžeme vidět, že velikost obvodové rychlosti se opravdu mění.
\begin{align*}
    v(t) &= r \cdot \omega(t)
\end{align*}


\subsubsection{Vektor zrychlení}

Abych zjistil vektor zrychlení, opět zderivuji vektor rychlosti a výsledek upravím.
\begin{align*}
    x''(t) &=          -  r \varepsilon(t) \cdot \sin\varphi(t) - r (\omega(t))^{2} \cdot \cos\varphi(t) \\
    y''(t) &= \phantom{-} r \varepsilon(t) \cdot \cos\varphi(t) - r (\omega(t))^{2} \cdot \sin\varphi(t) \\
    \vect{a}(t) &= r \varepsilon(t) \cdot
        \begin{pmatrix}[1.2]
            \,          -  \sin\varphi(t) \, \\
            \, \phantom{-} \cos\varphi(t) \,
        \end{pmatrix}
        + r (\omega(t))^{2} \cdot
        \begin{pmatrix}[1.2]
            \, - \cos\varphi(t) \, \\
            \, - \sin\varphi(t) \,
        \end{pmatrix}
\end{align*}
Oproti předchozímu odvození je zde výsledkem součet dvou různých vektorů.
Můžeme si všimnout, že první sčítanec má směr shodný s~vektor rychlosti, viz obr. \ref{fig:int2:zrychleni}.
Tato složka se nazývá \textit{tečným zrychlením} \(\vect{a_{t}}\) a mění velikost rychlosti tělesa, nikoliv však její směr.
Zbylá složka odpovídá \textit{dostředivému zrychlení} \(\vect{a_{d}}\) z~minulého odvození.
Ta naopak bude měnit pouze směr vektoru rychlosti, nikoliv však jeho velikost.
\begin{figure}[h]\centering{}
    \input{obrazky/zrychleny-zrychleni} \caption{Tečné a dostředivé zrychlení} \label{fig:int2:zrychleni}
\end{figure}

Rozdělení můžeme zapsat následovně:
\begin{align*}
    \vect{a}(t) &= \vect{a_{t}}(t) + \vect{a_{d}}(t); \,\,\vect{a_{t}}\,\bot\,\vect{a_{d}} \\
    \vect{a_{t}}(t) &= r \varepsilon(t) \cdot
        \begin{pmatrix}[1.2]
            \,          -  \sin\varphi(t) \, \\
            \, \phantom{-} \cos\varphi(t) \,
        \end{pmatrix} \\
    \vect{a_{d}}(t) &= r (\omega(t))^{2} \cdot
        \begin{pmatrix}[1.2]
            \, - \cos\varphi(t) \, \\
            \, - \sin\varphi(t) \,
        \end{pmatrix}
\end{align*}
Velikosti tečného a dostředivého zrychlení pak jsou následující.
\begin{align*}
    a_{t}(t) &= r \cdot \varepsilon(t) \\
    a_{d}(t) &= r \cdot (\omega(t))^{2}
\end{align*}


%  ____                                                                               _ _
% |  _ \ _ __ ___  _ __ ___   ___ _ __  _ __  _   _   _ __  _ __ _   ___   _____   __| (_) ___
% | |_) | '__/ _ \| '_ ` _ \ / _ \ '_ \| '_ \| | | | | '_ \| '__| | | \ \ / / _ \ / _` | |/ __|
% |  __/| | | (_) | | | | | |  __/ | | | | | | |_| | | |_) | |  | |_| |\ V / (_) | (_| | | (__
% |_|   |_|  \___/|_| |_| |_|\___|_| |_|_| |_|\__, | | .__/|_|   \__,_| \_/ \___/ \__,_|_|\___|
%                                             |___/  |_|

\subsection{Proměnlivá délka průvodiče}

Kromě rychlosti otáčení se může ale měnit i vzdálenost obíhajícího bodu od středu otáčení.
K~tomu může dojít, pokud se těleso bude pohybovat příliš rychle a dostředivá síla nebude dostatečně vělká.
Situace je načtrnutá na obr. \ref{fig:int3:pruvodic}.

\begin{figure}[h]\centering{}
    \input{obrazky/odstredivy-pruvodic} \caption{Proměnlivá délka průvodiče} \label{fig:int3:pruvodic}
\end{figure}

\subsubsection{Polohový vektor}

Úhlovou rychlost převezmu z~minulého odvození. Poloměr otáčení si opět definuji jako funkci závislou na čase. Zde však budu potřebovat, aby v~bodě
\(t\) existovala první a druhá derivace.
\begin{align*}
    r(t)&\colon \reals \rightarrow \reals \\
    t \in \reals&\colon \exists r'(t) \land \exists r''(t)
\end{align*}
Zadefinujeme si první a druhou derivaci jako rychlost a zrychlení v~ose průvodiče.
\begin{align*}
    v_{n}(t) = r'(t) \\
    a_{n}(t) = r''(t)
\end{align*}
Vektor pozice tedy bude mít následující podobu.
\begin{align*}
    \vect{r}(t) &=
        \begin{pmatrix}[1.2]
            \,r(t)      \, \\
            \,\varphi(t)\,
        \end{pmatrix}
\end{align*}
Po převedení do kartézských souřadnic:
\begin{align*}
    x(t) &= r(t) \cos\varphi(t) \\
    y(t) &= r(t) \sin\varphi(t) \\
    \vect{r}(t) &= r(t) \cdot{}
        \begin{pmatrix}[1.2]
            \, \cos\varphi(t) \, \\
            \, \sin\varphi(t) \,
        \end{pmatrix}
\end{align*}

\subsubsection{Vektor rychlosti}

K~určení vektoru rychlosti zderivujeme souřadnice polohového vektoru. Po zderivování vyjdou tyto souřadnice:
\begin{align*}
    x'(t) &= r'(t) \cos\varphi(t) - r(t) \omega(t) \sin\varphi(t) \\
    y'(t) &= r'(t) \sin\varphi(t) + r(t) \omega(t) \cos\varphi(t)
\end{align*}
Výsledek opět zapíšeme jako dvojici vektorů.
Odstředivá složka \(\vect{v_{n}}\) míří podle znaménka buď od, nebo ke středu otáčení -- tímto je zohledněna proměnlivá délka průvodiče.
Tečná složka \(\vect{v_{t}}\) míří vždy ve směru otáčení.
\begin{align*}
    \vect{v}(t) &= \vect{v_{n}}(t) + \vect{v_{t}}(t) \\
    \vect{v_{n}}(t) &= v_{n}(t) \cdot
        \begin{pmatrix}[1.2]
            \,\cos\varphi(t)\, \\
            \,\sin\varphi(t)\,
        \end{pmatrix} \\
    \vect{v_{t}}(t) &= r(t) \omega(t) \cdot
        \begin{pmatrix}[1.2]
            \,         -  \sin\varphi(t)\, \\
            \,\phantom{-} \cos\varphi(t)\,
        \end{pmatrix}
\end{align*}

\subsubsection{Vektor zrychlení}

Dále zderivujeme souřadnice vektoru rychlosti. Pro přehlednost jsem vynechal argumenty funkcí závislých na čase.
\begin{align*}
    x'' &= a_{n} \cos\varphi - v_{n} \omega \sin\varphi - v_{n} \omega \sin\varphi - r \varepsilon \sin\varphi - r \omega^{2} \cos\varphi \\
    y'' &= a_{n} \sin\varphi + v_{n} \omega \cos\varphi + v_{n} \omega \cos\varphi + r \varepsilon \cos\varphi - r \omega^{2} \sin\varphi \\
    % wtf, this is getting long
    \vect{a} &= \big(r \omega^{2} - a_{n}\big) \cdot
        \begin{pmatrix}[1.2]
            \,-\cos\varphi\, \\
            \,-\sin\varphi\,
        \end{pmatrix}
        + \big(2v_{n} \omega + r \varepsilon\big) \cdot
        \begin{pmatrix}[1.2]
            \,-\sin\varphi\, \\
            \,+\cos\varphi\,
        \end{pmatrix}
\end{align*}
Sčítance opět můžeme přesunout do vlastních veličin.
\begin{align*}
    \vect{a_{d}} &= \big(r \omega^{2} - a_{n}\big) \cdot
        \begin{pmatrix}[1.2]
            \,-\cos\varphi\, \\
            \,-\sin\varphi\,
        \end{pmatrix} \\
    \vect{a_{t}} &= \big(2v_{n} \omega + r \varepsilon\big) \cdot
        \begin{pmatrix}[1.2]
            \,-\sin\varphi\, \\
            \,+\cos\varphi\,
        \end{pmatrix}
\end{align*}
Výsledek můžeme také zapsat s~původními derivacemi.
\begin{align*}
    \vect{a_{d}} &= \big(r \varphi'^{2} - r''\big) \cdot
        \begin{pmatrix}[1.2]
            \,-\cos\varphi\, \\
            \,-\sin\varphi\,
        \end{pmatrix} \\
    \vect{a_{t}} &= \big(2r' \varphi' + r \varphi''\big) \cdot
        \begin{pmatrix}[1.2]
            \,-\sin\varphi\, \\
            \,+\cos\varphi\,
        \end{pmatrix}
\end{align*}

\subsubsection{Rovnoměrný přímočarý pohyb}

V~tuto chvíli mě napadlo, že nyní již dokážeme popsat pohyb bodu po přímce. Situace je znázorněná na obr. \ref{fig:int3:primocary}.

Budeme uvažovat bod A, který provádí rovnoměrný přímočarý pohyb po přímce p. Přímka p je ve vzdálenosti \(l\) od středu
otáčení S. Bod se pohybuje stálou rychlostí \(v\) a v~čase \(t_{0}\) se nachází na patě kolmice Sp.

\begin{figure}[h]\centering{}
    \input{obrazky/primocary} \caption{Rovnoměrný přímočarý pohyb} \label{fig:int3:primocary}
\end{figure}

Pak můžeme pohyb popsat následovně:
\begin{align*}
    s&= v\cdot t \\
    r &= \sqrt{l^{2} + s^{2}} \\
    \varphi &= \arctan{\frac{r}{l}}
\end{align*}
Připravíme si funkce pro dosazení:
\begin{align*}
    r   &= \sqrt{l^2 + (vt)^2} \\
    r'  &= \frac{v^2 t}{\sqrt{l^2+(v t)^2}}\\
    r'' &= \frac{(l v)^2}{\left(\sqrt{l^2+(v t)^2}\right)^{3}}\\
    \varphi   &= \arctan{\frac{v \cdot t}{l}} \\
    \varphi'  &= \frac{l v}{l^2+(v t)^2}\\
    \varphi'' &= - \frac{2 l v^3 t}{\left(l^2+(v t)^2\right)^2}
\end{align*}
Zajímat nás zatím budou pouze velikosti složek zrychlení. Můžeme dosadit:
\begin{align*}
    a_{d} &= \sqrt{l^2 + (vt)^2} \left(\frac{l v}{l^2+(vt)^2}\right)^{2} - \frac{(l v)^2}{\left(\sqrt{l^2+(vt)^2}\right)^{3}} \\
    a_{t} &= 2 \frac{v^2 t}{\sqrt{l^2+(vt)^2}} \frac{l v}{l^2+(vt)^2} - \sqrt{l^2 + (v~t)^2} \frac{2 l v^3 t}{\left(l^2+(vt)^2\right)^2}
\end{align*}
Zjednodušíme:
\begin{align*}
    a_{d} &= \frac{(l v)^2}{\left(\sqrt{l^2+(vt)^2}\right)^3} - \frac{(l v)^2}{\left(\sqrt{l^2+(vt)^2}\right)^{3}} = 0 \\
    a_{t} &= \frac{2 l v^3 t}{\left(\sqrt{l^2+(vt)^2}\right)^3} - \frac{2 l v^3 t}{\left(\sqrt{l^2+(vt)^2}\right)^3} = 0
\end{align*}

Můžeme vidět, že obě složky zrychlení vyšly nulové. Výsledek však dává smysl. Pokud se těleso pohybuje rovnoměrně přímočaře,
je výslednice sil na něj působících nulová. To znamená, že nulové je i jeho celkové zrychlení \(\vect{a}\). Protože složkové
vektory jsou na sebe kolmé, musí být tím pádem velikosti obou složek \(a_d\) a \(a_t\) nulové.
