SVG_IMAGES = $(wildcard obrazky/*.svg)
TEX_IMAGES = $(SVG_IMAGES:.svg=.tex)
TEX_PARTS = $(wildcard *.tex)

.PHONY: all clean view document.pdf

all: document.pdf



obrazky/grafy/data.stamp: program/data2/dlouhe.csv program/data2/stredni2.csv program/data2/kratke2.csv
	touch $@

obrazky/grafy/libs.stamp: program/zpracovani/graf_lib.py program/zpracovani/fixup_lib.py
	touch $@

obrazky/grafy/tecne.stamp: program/zpracovani/tecne.py obrazky/grafy/libs.stamp obrazky/grafy/data.stamp
	@echo " [Python] " $<
	python3 program/zpracovani/tecne.py
	touch $@

obrazky/grafy/dostredive.stamp: program/zpracovani/dostredive.py obrazky/grafy/libs.stamp obrazky/grafy/data.stamp
	@echo " [Python] " $<
	python3 program/zpracovani/dostredive.py
	touch $@

document.pdf: document.tex obrazky/grafy/tecne.stamp obrazky/grafy/dostredive.stamp
	@echo " [LTX] " $<
	latexmk -bibtex- -pdf -use-make $<

document.tex: $(TEX_IMAGES) $(TEX_PARTS)

obrazky/%.tex: obrazky/%.svg
	@echo " [SVG] " $<
	svg2tikz $< -t math --figonly -o $@

obrazky/komunikace-schema.tex: obrazky/komunikace-schema.svg
	@echo " [SVG] " $<
	svg2tikz $< -t raw --figonly -o $@

clean:
	latexmk -C document.tex
	rm -f *.aux obrazky/*.tex obrazky/*.aux obrazky/grafy/*

view: document.pdf
	atril document.pdf
