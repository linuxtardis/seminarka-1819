EESchema Schematic File Version 4
LIBS:merak-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Meřící zařízení"
Date "2019-04-21"
Rev "1"
Comp "Jakub Vaněk"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Knihovna:Arduino_Micro MCU1
U 1 1 5CBD050E
P 3800 6450
F 0 "MCU1" H 4200 6264 50  0000 C CNN
F 1 "Arduino Micro" H 4200 6151 79  0000 C CNN
F 2 "" H 3850 8350 50  0001 C CNN
F 3 "" H 3850 8350 50  0001 C CNN
	1    3800 6450
	1    0    0    -1  
$EndComp
$Comp
L Knihovna:GY-521 IMU1
U 1 1 5CBD05A5
P 9000 1800
F 0 "IMU1" V 9404 2527 50  0000 L CNN
F 1 "GY-521" V 9495 2527 50  0000 L CNN
F 2 "" H 9800 2550 50  0001 C CNN
F 3 "" H 9800 2550 50  0001 C CNN
	1    9000 1800
	0    1    1    0   
$EndComp
$Comp
L Knihovna:HC-05 BT1
U 1 1 5CBD0660
P 2000 2700
F 0 "BT1" H 2606 3565 50  0000 C CNN
F 1 "HC-05" H 2606 3474 50  0000 C CNN
F 2 "" H 2800 3450 50  0001 C CNN
F 3 "" H 2800 3450 50  0001 C CNN
	1    2000 2700
	1    0    0    -1  
$EndComp
$Comp
L Knihovna:Level_Shifter LS2
U 1 1 5CBD0735
P 4150 2550
F 0 "LS2" H 4850 2350 50  0000 C CNN
F 1 "Level_Shifter" H 4850 2450 50  0000 C CNN
F 2 "" H 4750 3200 50  0001 C CNN
F 3 "" H 4750 3200 50  0001 C CNN
	1    4150 2550
	1    0    0    -1  
$EndComp
$Comp
L Knihovna:Level_Shifter LS1
U 1 1 5CBD079B
P 8350 1950
F 0 "LS1" H 8350 2750 50  0000 C CNN
F 1 "Level_Shifter" H 8350 2650 50  0000 C CNN
F 2 "" H 8950 2600 50  0001 C CNN
F 3 "" H 8950 2600 50  0001 C CNN
	1    8350 1950
	-1   0    0    1   
$EndComp
$Comp
L power:+5V #PWR0101
U 1 1 5CBD130F
P 4850 4250
F 0 "#PWR0101" H 4850 4100 50  0001 C CNN
F 1 "+5V" H 4865 4423 50  0000 C CNN
F 2 "" H 4850 4250 50  0001 C CNN
F 3 "" H 4850 4250 50  0001 C CNN
	1    4850 4250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 5CBD1347
P 4850 4350
F 0 "#PWR0102" H 4850 4100 50  0001 C CNN
F 1 "GND" H 4855 4177 50  0000 C CNN
F 2 "" H 4850 4350 50  0001 C CNN
F 3 "" H 4850 4350 50  0001 C CNN
	1    4850 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	4850 4350 4350 4350
Wire Wire Line
	4850 4250 4150 4250
NoConn ~ 4250 4500
NoConn ~ 4050 4500
NoConn ~ 3700 4750
NoConn ~ 3700 4850
NoConn ~ 4150 6550
NoConn ~ 4250 6550
NoConn ~ 4700 6350
NoConn ~ 4700 6250
NoConn ~ 4700 6150
NoConn ~ 4700 6050
NoConn ~ 4700 5950
NoConn ~ 4700 5850
NoConn ~ 4700 5350
NoConn ~ 4700 5450
NoConn ~ 4700 5550
NoConn ~ 4700 5650
NoConn ~ 4700 4750
NoConn ~ 4700 4850
Text GLabel 4850 5050 2    50   Output ~ 0
MCU_BT_TX
Text GLabel 4850 5150 2    50   Input ~ 0
MCU_BT_RX
Wire Wire Line
	4700 5050 4850 5050
Wire Wire Line
	4850 5150 4700 5150
$Comp
L Device:LED D1
U 1 1 5CBD2131
P 3450 6350
F 0 "D1" H 3441 6095 50  0000 C CNN
F 1 "LED" H 3441 6186 50  0000 C CNN
F 2 "" H 3450 6350 50  0001 C CNN
F 3 "~" H 3450 6350 50  0001 C CNN
	1    3450 6350
	1    0    0    1   
$EndComp
$Comp
L power:GND #PWR0103
U 1 1 5CBD2553
P 2800 6450
F 0 "#PWR0103" H 2800 6200 50  0001 C CNN
F 1 "GND" H 2805 6277 50  0000 C CNN
F 2 "" H 2800 6450 50  0001 C CNN
F 3 "" H 2800 6450 50  0001 C CNN
	1    2800 6450
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 5CBD26DD
P 3050 6350
F 0 "R1" V 2843 6350 50  0000 C CNN
F 1 "R" V 2934 6350 50  0000 C CNN
F 2 "" V 2980 6350 50  0001 C CNN
F 3 "~" H 3050 6350 50  0001 C CNN
	1    3050 6350
	0    1    1    0   
$EndComp
NoConn ~ 3700 6250
NoConn ~ 3700 6150
NoConn ~ 3700 6050
NoConn ~ 3700 5950
Text GLabel 3550 5850 0    50   Input ~ 0
MCU_IMU_INT
Text GLabel 3550 5650 0    50   Output ~ 0
MCU_IMU_5V
Text GLabel 3550 5550 0    50   Input ~ 0
MCU_BT_STATE
Text GLabel 3550 5450 0    50   Output ~ 0
MCU_BT_MODE
Text GLabel 3550 5250 0    50   BiDi ~ 0
MCU_IMU_SCL
Text GLabel 3550 5150 0    50   BiDi ~ 0
MCU_IMU_SDA
Wire Wire Line
	3550 5850 3700 5850
Wire Wire Line
	3550 5650 3700 5650
Wire Wire Line
	3700 5550 3550 5550
Wire Wire Line
	3550 5450 3700 5450
Wire Wire Line
	3550 5250 3700 5250
Wire Wire Line
	3700 5150 3550 5150
NoConn ~ 3700 5750
Wire Wire Line
	4150 4250 4150 4500
Wire Wire Line
	4350 4350 4350 4500
$Comp
L power:GND #PWR0104
U 1 1 5CBD53E9
P 7300 5100
F 0 "#PWR0104" H 7300 4850 50  0001 C CNN
F 1 "GND" H 7305 4927 50  0000 C CNN
F 2 "" H 7300 5100 50  0001 C CNN
F 3 "" H 7300 5100 50  0001 C CNN
	1    7300 5100
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0105
U 1 1 5CBD5427
P 7300 4200
F 0 "#PWR0105" H 7300 4050 50  0001 C CNN
F 1 "+5V" H 7315 4373 50  0000 C CNN
F 2 "" H 7300 4200 50  0001 C CNN
F 3 "" H 7300 4200 50  0001 C CNN
	1    7300 4200
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0106
U 1 1 5CBD550D
P 7600 4300
F 0 "#PWR0106" H 7600 4150 50  0001 C CNN
F 1 "+3.3V" H 7615 4473 50  0000 C CNN
F 2 "" H 7600 4300 50  0001 C CNN
F 3 "" H 7600 4300 50  0001 C CNN
	1    7600 4300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0107
U 1 1 5CBD554B
P 9450 5200
F 0 "#PWR0107" H 9450 4950 50  0001 C CNN
F 1 "GND" H 9455 5027 50  0000 C CNN
F 2 "" H 9450 5200 50  0001 C CNN
F 3 "" H 9450 5200 50  0001 C CNN
	1    9450 5200
	1    0    0    -1  
$EndComp
Text GLabel 9050 4250 0    50   Input ~ 0
MCU_IMU_5V
Text GLabel 9600 4250 2    50   Output ~ 0
IMU_5V
Text GLabel 9950 4700 2    50   Output ~ 0
IMU_3V3
$Comp
L Device:R R2
U 1 1 5CBD57E4
P 7300 4450
F 0 "R2" H 7370 4496 50  0000 L CNN
F 1 "120" H 7370 4405 50  0000 L CNN
F 2 "" V 7230 4450 50  0001 C CNN
F 3 "~" H 7300 4450 50  0001 C CNN
	1    7300 4450
	1    0    0    -1  
$EndComp
$Comp
L Device:R R3
U 1 1 5CBD583E
P 7300 4850
F 0 "R3" H 7370 4896 50  0000 L CNN
F 1 "220" H 7370 4805 50  0000 L CNN
F 2 "" V 7230 4850 50  0001 C CNN
F 3 "~" H 7300 4850 50  0001 C CNN
	1    7300 4850
	1    0    0    -1  
$EndComp
Wire Wire Line
	7300 4200 7300 4300
Wire Wire Line
	7300 4600 7300 4650
Wire Wire Line
	7300 5000 7300 5100
Connection ~ 7300 4650
Wire Wire Line
	7300 4650 7300 4700
Wire Wire Line
	7300 4650 7600 4650
Wire Wire Line
	7600 4650 7600 4300
$Comp
L Device:R R4
U 1 1 5CBD901B
P 9450 4500
F 0 "R4" H 9520 4546 50  0000 L CNN
F 1 "100" H 9520 4455 50  0000 L CNN
F 2 "" V 9380 4500 50  0001 C CNN
F 3 "~" H 9450 4500 50  0001 C CNN
	1    9450 4500
	1    0    0    -1  
$EndComp
$Comp
L Device:R R5
U 1 1 5CBD90A7
P 9450 4900
F 0 "R5" H 9520 4946 50  0000 L CNN
F 1 "220" H 9520 4855 50  0000 L CNN
F 2 "" V 9380 4900 50  0001 C CNN
F 3 "~" H 9450 4900 50  0001 C CNN
	1    9450 4900
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 5CBDAC16
P 9800 4900
F 0 "C1" H 9915 4946 50  0000 L CNN
F 1 "22n" H 9915 4855 50  0000 L CNN
F 2 "" H 9838 4750 50  0001 C CNN
F 3 "~" H 9800 4900 50  0001 C CNN
	1    9800 4900
	1    0    0    -1  
$EndComp
$Comp
L Device:C C2
U 1 1 5CBDACB8
P 9200 4700
F 0 "C2" H 9085 4654 50  0000 R CNN
F 1 "22n" H 9085 4745 50  0000 R CNN
F 2 "" H 9238 4550 50  0001 C CNN
F 3 "~" H 9200 4700 50  0001 C CNN
	1    9200 4700
	1    0    0    1   
$EndComp
Wire Wire Line
	9450 4750 9450 4700
Wire Wire Line
	9200 5100 9450 5100
Connection ~ 9450 5100
Wire Wire Line
	9200 4550 9200 4250
Wire Wire Line
	9200 4250 9450 4250
Connection ~ 9450 4700
Wire Wire Line
	9450 4700 9450 4650
Wire Wire Line
	9450 4700 9800 4700
Wire Wire Line
	9800 4750 9800 4700
Wire Wire Line
	9450 5100 9800 5100
Wire Wire Line
	9800 5050 9800 5100
Wire Wire Line
	9200 4850 9200 5100
Wire Wire Line
	9450 5100 9450 5200
Wire Wire Line
	9450 5050 9450 5100
Wire Wire Line
	9450 4250 9450 4350
Wire Wire Line
	9050 4250 9200 4250
Connection ~ 9200 4250
Wire Wire Line
	9450 4250 9600 4250
Connection ~ 9450 4250
Wire Wire Line
	9800 4700 9950 4700
Connection ~ 9800 4700
NoConn ~ 8900 2500
NoConn ~ 8900 2400
NoConn ~ 8900 2300
$Comp
L power:GND #PWR0108
U 1 1 5CBF380D
P 7950 2650
F 0 "#PWR0108" H 7950 2400 50  0001 C CNN
F 1 "GND" H 7955 2477 50  0000 C CNN
F 2 "" H 7950 2650 50  0001 C CNN
F 3 "" H 7950 2650 50  0001 C CNN
	1    7950 2650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0109
U 1 1 5CBF3856
P 7750 1700
F 0 "#PWR0109" H 7750 1450 50  0001 C CNN
F 1 "GND" H 7755 1527 50  0000 C CNN
F 2 "" H 7750 1700 50  0001 C CNN
F 3 "" H 7750 1700 50  0001 C CNN
	1    7750 1700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0110
U 1 1 5CBF389F
P 9050 1550
F 0 "#PWR0110" H 9050 1300 50  0001 C CNN
F 1 "GND" H 9055 1377 50  0000 C CNN
F 2 "" H 9050 1550 50  0001 C CNN
F 3 "" H 9050 1550 50  0001 C CNN
	1    9050 1550
	1    0    0    -1  
$EndComp
Wire Wire Line
	7950 1700 7950 1850
Wire Wire Line
	7750 1700 7950 1700
Wire Wire Line
	8900 2000 8850 2000
Wire Wire Line
	8850 2000 8850 1550
Wire Wire Line
	8850 1550 9050 1550
Text GLabel 8750 1900 0    50   Input ~ 0
IMU_5V
Text GLabel 7950 2950 0    50   Input ~ 0
IMU_3V3
Text GLabel 7950 1600 0    50   Input ~ 0
IMU_5V
Wire Wire Line
	7950 1600 8050 1600
Wire Wire Line
	8050 1600 8050 1850
Wire Wire Line
	8050 2950 8050 2650
Wire Wire Line
	7950 2950 8050 2950
Wire Wire Line
	8750 1900 8900 1900
Wire Wire Line
	8900 2600 8800 2600
Wire Wire Line
	8800 2600 8800 2400
Wire Wire Line
	8800 2400 8450 2400
Wire Wire Line
	8450 2300 8800 2300
Wire Wire Line
	8800 2300 8800 2200
Wire Wire Line
	8800 2200 8900 2200
Wire Wire Line
	8900 2100 8700 2100
Wire Wire Line
	8700 2100 8700 2200
Wire Wire Line
	8700 2200 8450 2200
NoConn ~ 8450 2100
NoConn ~ 7550 2100
Text GLabel 7400 2400 0    50   Output ~ 0
MCU_IMU_INT
Text GLabel 7400 2300 0    50   BiDi ~ 0
MCU_IMU_SDA
Text GLabel 7400 2200 0    50   BiDi ~ 0
MCU_IMU_SCL
Wire Wire Line
	7400 2400 7550 2400
Wire Wire Line
	7400 2300 7550 2300
Wire Wire Line
	7400 2200 7550 2200
$Comp
L power:GND #PWR0111
U 1 1 5CC037D8
P 4150 3200
F 0 "#PWR0111" H 4150 2950 50  0001 C CNN
F 1 "GND" H 4155 3027 50  0000 C CNN
F 2 "" H 4150 3200 50  0001 C CNN
F 3 "" H 4150 3200 50  0001 C CNN
	1    4150 3200
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0112
U 1 1 5CC03828
P 4150 2800
F 0 "#PWR0112" H 4150 2650 50  0001 C CNN
F 1 "+5V" H 4165 2973 50  0000 C CNN
F 2 "" H 4150 2800 50  0001 C CNN
F 3 "" H 4150 2800 50  0001 C CNN
	1    4150 2800
	1    0    0    -1  
$EndComp
Text GLabel 4950 2300 2    50   Input ~ 0
MCU_BT_TX
Text GLabel 4950 2200 2    50   Output ~ 0
MCU_BT_RX
Text GLabel 4950 2400 2    50   Output ~ 0
MCU_BT_STATE
Text GLabel 4950 2100 2    50   Input ~ 0
MCU_BT_MODE
$Comp
L power:+3.3V #PWR0113
U 1 1 5CC03E81
P 4850 1300
F 0 "#PWR0113" H 4850 1150 50  0001 C CNN
F 1 "+3.3V" H 4865 1473 50  0000 C CNN
F 2 "" H 4850 1300 50  0001 C CNN
F 3 "" H 4850 1300 50  0001 C CNN
	1    4850 1300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0114
U 1 1 5CC03ED1
P 4850 1700
F 0 "#PWR0114" H 4850 1450 50  0001 C CNN
F 1 "GND" H 4855 1527 50  0000 C CNN
F 2 "" H 4850 1700 50  0001 C CNN
F 3 "" H 4850 1700 50  0001 C CNN
	1    4850 1700
	1    0    0    -1  
$EndComp
$Comp
L Device:C C3
U 1 1 5CC04130
P 4850 1500
F 0 "C3" H 4965 1546 50  0000 L CNN
F 1 "22n" H 4965 1455 50  0000 L CNN
F 2 "" H 4888 1350 50  0001 C CNN
F 3 "~" H 4850 1500 50  0001 C CNN
	1    4850 1500
	1    0    0    -1  
$EndComp
$Comp
L Device:C C4
U 1 1 5CC0418C
P 4150 3000
F 0 "C4" H 4265 3046 50  0000 L CNN
F 1 "22n" H 4265 2955 50  0000 L CNN
F 2 "" H 4188 2850 50  0001 C CNN
F 3 "~" H 4150 3000 50  0001 C CNN
	1    4150 3000
	1    0    0    -1  
$EndComp
$Comp
L Device:C C5
U 1 1 5CC041FD
P 3100 3200
F 0 "C5" H 3215 3246 50  0000 L CNN
F 1 "22n" H 3215 3155 50  0000 L CNN
F 2 "" H 3138 3050 50  0001 C CNN
F 3 "~" H 3100 3200 50  0001 C CNN
	1    3100 3200
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0115
U 1 1 5CC084E0
P 3100 3000
F 0 "#PWR0115" H 3100 2850 50  0001 C CNN
F 1 "+5V" H 3115 3173 50  0000 C CNN
F 2 "" H 3100 3000 50  0001 C CNN
F 3 "" H 3100 3000 50  0001 C CNN
	1    3100 3000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0116
U 1 1 5CC08539
P 3100 3400
F 0 "#PWR0116" H 3100 3150 50  0001 C CNN
F 1 "GND" H 3105 3227 50  0000 C CNN
F 2 "" H 3100 3400 50  0001 C CNN
F 3 "" H 3100 3400 50  0001 C CNN
	1    3100 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	4150 2850 4450 2850
Wire Wire Line
	4450 2850 4450 2650
Wire Wire Line
	4150 3150 4550 3150
Wire Wire Line
	4550 3150 4550 2650
Wire Wire Line
	4550 1650 4850 1650
Wire Wire Line
	4850 1350 4450 1350
Wire Wire Line
	3100 3050 3300 3050
Wire Wire Line
	3300 3050 3300 2200
Wire Wire Line
	3100 3350 3400 3350
Wire Wire Line
	3400 3350 3400 2300
Wire Wire Line
	3300 2200 3200 2200
Wire Wire Line
	3400 2300 3200 2300
Wire Wire Line
	3200 2400 3500 2400
Wire Wire Line
	3500 2400 3500 2200
Wire Wire Line
	3200 2500 3600 2500
Wire Wire Line
	3600 2500 3600 2300
Wire Wire Line
	3200 2600 3700 2600
Wire Wire Line
	3700 2600 3700 2400
Wire Wire Line
	4850 1700 4850 1650
Connection ~ 4850 1650
Wire Wire Line
	4850 1300 4850 1350
Connection ~ 4850 1350
Wire Wire Line
	4450 1350 4450 1850
Wire Wire Line
	4550 1650 4550 1850
Wire Wire Line
	3100 3050 3100 3000
Connection ~ 3100 3050
Wire Wire Line
	3100 3350 3100 3400
Connection ~ 3100 3350
Wire Wire Line
	3200 2100 4050 2100
Wire Wire Line
	4050 2200 3500 2200
Wire Wire Line
	3600 2300 4050 2300
Wire Wire Line
	4050 2400 3700 2400
Wire Wire Line
	3700 6350 3600 6350
Wire Wire Line
	3300 6350 3200 6350
Wire Wire Line
	2900 6350 2800 6350
Wire Wire Line
	2800 6350 2800 6450
Wire Wire Line
	4150 2800 4150 2850
Connection ~ 4150 2850
Wire Wire Line
	4150 3200 4150 3150
Connection ~ 4150 3150
$EndSCHEMATC
