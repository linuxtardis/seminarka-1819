# Seminární práce 2018/2019 – „Odvození dostředivého zrychlení“

Souboru mají následující strukturu:
* Výstup
  - `/Odvození dostředivého zrychlení.pdf` - práce vyexportovaná do PDF
  - `/Odvození dostředivého zrychlení.pdf.sig` - PGP podpis práce (ID klíče 0xCCDE818F3F894D36)
* LaTeX
  - `/Makefile` - "lepidlo", které dokáže celou práci vyexportovat do PDF
  - `/document.tex` - hlavní soubor, který se předává LaTeXu. Slouží pro spojení částí a pro uklizení "nehezkých" částí.
  - `/odvozeni.tex` - obsahuje zápis odvození.
  - `/experiment.tex` - obsahuje zápis popisu experimentu.
  - `/mereni.tex` - obsahuje zápis výsledků měření.
  - `/zaver.tex` - obsahuje zápis závěru.
  - `/biblio.tex` - obsahuje ručně dělaný seznam zdrojů.
* Obrázky, schémata
  - `/obrazky/*.jpg` - fotografie použité v práci.
  - `/obrazky/*.svg` - nákresy z programu Inkscape. Do práce se vloží jako TikZový kód po převedení programem [svg2tikz](https://github.com/kjellmf/svg2tikz).
  - `/obrazky/schema.pdf` - vyexportované schéma zapojení měřícího zařízení.
  - `/schema` - schéma zapojení měřícího zařízení ve formátu programu KiCad.
* Programy
  - `/program/arduino` - program běžící na meřícím zařízení
  - `/program/IMU_Zero` - program pro kalibraci meřícího zařízení
  - `/program/data2` - použitá naměřená data
  - `/program/data`, `/program/data3` - nepoužitá naměřená data
  - `/program/ev3` - program běžící na EV3, který řídí otáčky plošiny
  - `/program/pc` - program běžící na počítači, který ovládá průběh pokusu
  - `/program/zpracovani` - programy pro zpracování a vykreslení dat do PDF grafů, které jsou pak vloženy do práce
