\section{Měřící zařízení}
V~této kapitole popíšu zařízení použité k~měření a jakým způsobem jsem zpracoval naměřená data.

Za základní požadavky pro návrh systému jsem považoval to, aby pokus šel jednoduše automatizovat a také v~případě potřeby zopakovat.
Mojí první volbou proto bylo vyrobit ovládatelnou plošinu, která by se dokázala otáčet předem určenou úhlovou rychlostí a s~určeným zrychlením.
Na plošině by pak byla umístěná samotná měřící sonda, která by snímala dostředivé zrychlení v~určité vzdálenosti od středu otáčení.
Sonda by pak data mohla buď sama ukládat, nebo je za běhu odesílat na zpracování počítači. Po dokončení měření bych výsledky porovnal
s~předpokládanými hodnotami.

\subsection{Hardware}
\subsubsection{Otočná plošina}
Otáčecí zařízení jsem se rozhodl postavit ze stavebnice LEGO Mindstorms EV3 Education (více v~\cite{wikiEV3}).
Ta se v~zásadě skládá ze čtyř částí: vybraných dílků LEGO Technic, programovatelné kostky EV3 a pak kompatibilních senzorů a servomotorů.
To s~sebou přineslo určité výhody:
\begin{itemize}
\item Proces sestrojení fyzické plošinky byl relativně jednoduchý a přímočarý.
\item Na EV3 běží plnohodnotný vestavěný Linuxový systém\footnotemark. Díky tomu jsem měl na výběr z~mnoha možností při programování a ladění.
      Také jsem nemusel nutně řešit přímou konfiguraci hardware, protože ta je obvykle vyřešená v~systémových ovladačích.
\item EV3 obsahuje oproti např. deskám Arduino relativně výkonný procesor. V~tomto případě nebyl ale výpočetní výkon omezující.
\item EV3 podporuje komunikaci přes bezdrátové rozhraní Bluetooth.
\item Díky účasti na různých akcích zaměřených na použití této stavebnici již mám zkušenosti, co by jak šlo udělat.
\end{itemize}
\footnotetext{Jedním takovým je systém \texttt{ev3dev}, \url{https://www.ev3dev.org/}.}
Použití této stavebnice mělo ale i nějaké zápory:
\begin{itemize}
\item Protože jednotlivé dílky stavebnice jsou plastové a nejsou k~sobě přišroubované ani přilepené, výsledná konstrukce se může mírně ohýbat.
      Použití Technic dílků mě také částečně omezilo v~tom, jak jdou součástky spojovat.
\item Při použití \uv{běžného} Linuxového systému může docházet k~nepravidelnostem v~řízení. \cite{wikiRTOS}
      To je způsobené tím, že Linux ve výchozím stavu nepodporuje tzv. plánování v~reálném čase.
      To by šlo obejít použitím nějakého RTOS (operačního systému reálného času), jako např. je EV3RT\footnotemark.
\footnotetext{\texttt {What is EV3RT?}, \url{http://ev3rt-git.github.io/about/}}
\end{itemize}

Jednou z~alternativ bylo použít desku typu Arduino a k~ní připojit motor se senzorem otáček.
\begin{itemize}
\item Díky tomu by řízení mohlo probíhat v~reálném čase, protože by na zařízení běžel pouze jeden program.
      V~něm by mělo jít zajistit co nejnižší zpoždění řídící odezvy.
\item Použitím jiného stavebního základu bych mohl docílit pevnější a lehčí konstrukce.
\item Bluetooth bych mohl využívat také, musel bych k~tomu ale využít externí modul.
\item Programování by mohlo být i nadále relativně jednoduché, pokud by byly dostupné potřebné knihovny.
\end{itemize}
Na druhou stranu:
\begin{itemize}
\item V~danou chvíli jsem neměl dostupné potřebné motory a senzory k~tomu, abych dokázal ovládat otáčení plošiny.
\item Taktéž jsem neměl k~dispozici ani způsob, jakým bych nahradil LEGO Technic ke stavbě konstrukce.
\item Podobný systém jsem zatím nesestavoval. Ačkoliv bych se naučil nové věci, přínosy tohoto řešení by nejspíš nevyvážily potřebný čas.
\end{itemize}

Pro první prototyp jsem si vybral právě EV3, protože jsem tuto sadu již dobře znal.
Při jeho používání jsem nenarazil na větší problémy, nebylo tedy potřeba jej výrazně předělávat.
Negativně se ale projevila volnost otáčecího ramene a konstrukce v~místě napojení plošiny na pohon.
To totiž mohlo způsobit zkreslení tečného zrychlení, viz měření v oddíle \ref{sec:tecne-zrychleni}.

Zařízení se skládá ze dvou částí. První je pasivní plošina, která drží měřící sondu a otáčí se s~ní, viz obr. \ref{fig:hw:prostredek}.
Plošina je pak připojena na spodní motorovou část, která řídí otáčení, viz obr. \ref{fig:hw:spodek}.
Součástí spodku je také ovládací kostka.
\begin{figure}[h]\centering{}
\includegraphics[height=90mm]{obrazky/motorova.JPG}
\caption{Motorová část}\label{fig:hw:spodek}
\end{figure}
\begin{figure}[h]\centering{}
\includegraphics[width=90mm]{obrazky/otaceci.JPG}
\caption{Otáčivá část s~měřící sondou}\label{fig:hw:prostredek}
\end{figure}

Spodní část jsem sestavoval tak, aby byla relativně pevná, ale zároveň aby měla díky dvěma motorům dostatečný výkon k~rychlému roztočení plošiny.
U~horní části pak bylo důležité zajistit pevné, ale lehké uchycení měřící sondy.

\subsubsection{Měřící sonda}
Zařízení umístěné na otočné plošině bylo nutné udělat bezdrátové. V opačném případě by se přívodní kabely po chvíli zamotaly do otáčecího zařízení.
To tedy vyloučilo použití senzoru pro EV3, který by byl připojený přímo k~řídící kostce.
Jednou z~variant by bylo použít druhou sadu EV3, nevýhodou je ale velká hmotnost samotné řídící kostky.
Výchozí sada také neobsahuje akcelerometr, který bych musel sehnat zvlášť.

Napadly mě tedy dvě možnosti -- použít již existující bezdrátový senzor, nebo si postavit vlastní.
Protože jsem chtěl mít zařízení pod svojí kontrolou, vydal jsem se cestou vlastního řešení.
Zde jsem naopak jako základ zvolil Arduino Micro, protože jsem jej již měl doma.
Také jsem už zhruba věděl, jak obvod sestavit a jak jej naprogramovat.

Aby zařízení splnilo svůj účel, musí umět měřit dostředivé zrychlení a ideálně také úhlovou rychlost, kterou se otáčí.
Proto jsem k~Arduinu pomocí sběrnice I2C připojil čip MPU-6050 na desce GY-521, který obsahuje tříosý gyroskop a tříosý akcelerometr.
Vhodným umístěním na rameni otáčecí plošiny jsem tedy mohl přibližně izolovat směry jednotlivých veličin.

Naměřená data bylo pak žádoucí buď uložit na zařízení, nebo je průběžně odesílat.
Pro ukládání by šlo použít např. microSD kartu, neboť by ji bylo možné připojit pomocí sběrnice SPI.
To by mi ale ztížilo automatizaci pokusu, rozhodl jsem se tedy pro bezdrátové ovládání.
Nejvhodnější mi přišlo k~tomuto účelu použít Bluetooth modul HC-05, který poskytuje emulovaný sériový port přes Bluetooth SPP (Serial Port Profile).
Stačilo jej připojit k~UART pinům na Arduinu a pak jej správně nastavit. K tomu jsem využil zdrojů \cite{hc05a}, \cite{hc05b}.
Zde se také vyplatilo použití Micro modelu Arduina. Díky přítomnosti nativního USB v~jeho procesoru bylo
možné použít uvolněný hardwarový sériový port a nikoliv jen jeho softwarovou emulaci.

K~napájení zařízení jsem použil malou USB power banku, protože to byl nejdostupnější způsob napájení.
Pouze jsem musel dát pozor, aby její umístění příliš nevychýlilo těžiště celé plošiny.

Většinu elektronických součástek jsem umístil na nepájivé pole.
Díky tomu jsem mohl zařízení rychle sestavit a také provádět úpravy zapojení.
Senzor samotný a jeho převodník jsem musel připojit přes kabelové propojky, abych dosáhl delšího možného ramene otáčení.

Zapojení je vyfocené na obrázku \ref{fig:hw:merak_soucastky}, zabudování měřícího zařízení do otočné plošiny na obr. \ref{fig:hw:merak_cely}.
Schéma obvodu jsem přiložil na poslední stránku této práce.
\begin{figure}[h]\centering{}
\includegraphics[width=90mm]{obrazky/merak-soucastky.JPG}
\caption{Nepájivé pole se zapojenými součástkami.}\label{fig:hw:merak_soucastky}
\end{figure}
\begin{figure}[h]\centering{}
\includegraphics[width=90mm]{obrazky/merak-cely.JPG}
\caption{Kompletní měřící plošina}\label{fig:hw:merak_cely}
\end{figure}

\subsubsection{Řídící počítač}

Pro ovládání pokusu jsem použil svůj notebook ThinkPad X200.
Pro komunikaci s~měřící sondou jsem využil interní Bluetooth,
pro ovládání motorové části plošiny jsem použil externí USB Bluetooth adaptér.

\subsection{Software}

Software běžící během pokusu se skládal ze čtyř hlavních částí, znázorněných na obrázku \ref{fig:sw:komunikace}.
\begin{figure}[h!]\centering{}
    \begingroup
        \fontsize{9}{10}
        \input{obrazky/komunikace-schema}
    \endgroup
    \caption{Schéma komunikace} \label{fig:sw:komunikace}
\end{figure}

\subsubsection{Ovládací skript}
Skript má za úkol dávat ovládací plošině ve správný čas určené povely.
K~tomu jsem využil programovacího jazyka Python a knihovny \texttt{pybluez}, viz zdroj \cite{pybluez}.
Jediným aktivním povelem byl příkaz k~roztočení plošiny ze současné rychlosti na rychlost \(\omega\) s~průměrným úhlovým zrychlením \(\varepsilon\).
Program byl napsaný tak, aby se průběh úhlové rychlosti blížil té na obrázku \ref{fig:sw:rampa}. Tento průběh byl spuštěn celkem pětkrát.

Původně měl skript na starost i čtení a rozdělování dat. S~tím jsem ale měl problémy, proto jsem tuto funkci řešil zvlášť.
\begin{figure}[h]\centering{}
    \begingroup
        \fontsize{9}{10}
        \input{obrazky/prubeh} \caption{Průběh velikosti úhlové rychlosti v~čase} \label{fig:sw:rampa}
    \endgroup
\end{figure}

\subsubsection{Ukládání dat}

Tuto funkcionalitu jsem vyřešil velmi jednoduše. Jediné, co počítač prováděl, je zápis přicházejícího textového proudu přímo do souboru, bez jakéhokoliv zpracování.
Toho jsem docílil pomocí programů \texttt{cat}\footnote{\url{https://linux.die.net/man/1/cat}} a \texttt{rfcomm}\footnote{\url{https://linux.die.net/man/1/rfcomm}}.

\subsubsection{Řízení plošiny}

Pro tento účel jsem upravil jeden z~mých dřívějších programů na řízení robota. Využíval jazyk C++ a operační systém \texttt{ev3dev}.
Výsledný kód čekal na připojení po Bluetooth SPP a pak přijímal příkazy. Těmi šlo nastavovat parametry pohybu plošiny.
Pro komunikaci přes Bluetooth jsem použil příkladové programy z \cite{cppbluez}.

Plošina byla nejprve řízena pomocí PID regulátoru, který měl zajistit, aby se plošina otáčela opravdu zadanou rychlostí.
Při prvních měřeních ale aktivní regulace způsobovala nechtěné překočení potřebné rychlosti a mohla také přispívat k~zašumění naměřených dat.
Proto jsem aktivní regulaci nahradil lineární funkcí, která každé rychlosti \(\omega\) přiřadila určitou hodnotu pulzně šířkové modulace, kterou byly řízené motory.
Pak jsem sice neměl přesně řízené otáčky, ale okamžitou úhlovou rychlost měřila i samotná sonda.

Abych dosáhnul plynulého přechodu mezi rychlostmi, vstup PID regulátoru (resp. pak lineární funkce) jsem měnil průběžně s~časem.
To by mi mělo umožnit zhruba porovnat působící tečné zrychlení.

\subsubsection{Meřící zařízení}

Poslední komponentou je měřící zařízení. Jeho rolí je pravidelně číst data ze senzoru a posílat je řídícímu počítači.

Pro komunikaci se senzorem MPU-6050 jsem použil knihovnu \texttt{i2cdevlib}, viz zdroj \cite{mpu6050}.
Jako základ jsem použil program \texttt{MPU6050 raw} dodávaný s knihovnou.

Pomocí API knihovny jsem nastavil, že čip má posílat nová data každých \(\SI{10}{\milli\second}\).
Dále jsem zvolil, že se data mají filtrovat přes dolní propust s~mezní frekvencí \(\SI{20}{\hertz}\).
Takto by mělo být omezeno množství vysokofrekvenčních změn, které nejsou zajímavé.
Tímto potenciálně zkresluji naměřená data, myslím si ale, že správně by žádné takové změny (kromě na začátku změny rychlosti) nastat neměly.

Jedním z~nastavitelných parametrů MPU je interní hodnota měření akcelerometru a gyroskopu, která bude považovaná za nulovou hodnotu.
Tyto offsety jsem zjišťoval pomocí programu \texttt{IMU Zero} poskytovaného taktéž s~použitou knihovnou.
Program se snažil najít takové offsety, pro které by platilo, že vodorovné složky zrychlení budou nulové a svislá složka zrychlení bude mířit dolů s velikostí \(\SI{1}{\gram}\). % not exactly a gram, but whatever
Tato procedura může do měření vnést nepřesnost, pokud klidová poloha není opravdu vodorovná.
Pro offsety gyroskopu kalibrační program hledal pouze místo s nulovými hodnotami, protože MPU při kalibraci zůstávalo v klidu.

Dalším z~parametrů je citlivost měření.
Zvolil jsem hodnoty \(\SI{\pm8}{\gram}\) a \(\SI{\pm1000}{\degree\per\second}\), protože pak se všechny naměřené hodnoty vešly do měřícího rozsahu.

Samotný odečet měření za běhu pokusu probíhal následovně.
Senzor pravidelně měřil nové hodnoty a procesor Arduina je na signál odečítal.
Po přečtení je upravil do požadovaných jednotek a pak je odeslal jako čárkou oddělené hodnoty hlavnímu počítači.
Tento formát pak bylo možné přímo ukládat do CSV\footnote{\textit{Comma-separated values}, čili hodnoty oddělené čárkou} souboru.

Každý řádek měl následující strukturu:
\[
t,~\omega_x,~\omega_y,~\omega_z,~a_x,~a_y,~a_z
\]
Čas \(t\) je časová známka v~milisekundách přidaná Arduinem.
Vektory \(\vect{\omega}\) a \(\vect{a}\) odpovídají naměřeným hodnotám úhlové rychlosti a zrychlení na jednotlivých osách senzoru.
Směr \(x\) odpovídá směru tečného zrychlení,       % X => tangent
směr \(y\) odpovídá směru dostředivého zrychlení a % Y => centripetal
směr \(z\) odpovídá směru tíhového zrychlení.      % Z => gravity

\subsection{Korekce dat}

Po prohlédnutí naměřených dat bylo znatelné, že se mi nepodařilo úplně zarovnat osy akcelerometru s~vodorovnou rovinou.
Například hodnoty zrychlení naměřené ve svislém (tíhovém) směru měly větší velikost, když se rameno otáčelo rychleji.
Je velmi nepravděpodobné, že otáčení ramene by změnilo velikost tíhového zrychlení.
Spíše je možné, že svislá osa akcelerometru byla nakloněná mírně takovým směrem, že snímala i dostředivé zrychlení.

Obdobná situace nastala u~hodnot úhlové rychlosti -- senzor se například nemohl otáčet podél osy ramene, ačkoliv meření by tomu odpovídalo.
Lepším vysvětlením je opět, že senzor snímal otáčky také z~jiné než vodorovné roviny, protože osa měření nebyla umístěna plně svisle.

Vytvořil jsem proto program v~Pythonu využívající knihovny \texttt{numpy}, \texttt{numpy-quaternion} a \texttt{pandas}, který některé z~těchto problémů odstraňuje.

Zpracování probíhá pouze na datech z~akcelerometru, protože z~gyroskopu je potřeba zjišťovat pouze málo zkreslenou rychlost okolo osy \textit{z}.

%\subsubsection{Korekce vektoru úhlové rychlosti}
%Jediná ne-šumová a ne-vibrační hodnota, která v~tomto pokusu dává smysl, je rychlost otáčení okolo svislé osy.
%Proto jsem vstupní naměřené vektory nahradil vektorem mířícím ve směru osy \textit{z}, který měl stejnou velikost jako vektor původní.
%\begin{align*}
%        \begin{pmatrix}[1.2]
%            \,\omega_x\, \\
%            \,\omega_y\, \\
%            \,\pm\omega_z\,
%        \end{pmatrix}
%        &\rightarrow
%        \begin{pmatrix}[1.2]
%            \,0\, \\
%            \,0\, \\
%            \,\pm\sqrt{\omega_x^2+\omega_y^2+\omega_z^2}\,
%        \end{pmatrix}
%\end{align*}

\label{sec:korekce}
\subsubsection{Korekce vektoru zrychlení}
Abych odstranil vzájemný vliv os, potřeboval jsem do dat zabudovat tyto předpoklady:
\begin{enumerate}
\item Pokud se plošina neotáčí, tečné zrychlení by mělo být nulové.
\item Pokud se plošina otáčí konstantní úhlovou rychlostí, tečné zrychlení by mělo být nulové.
\item Tíhové zrychlení by po celou dobu mělo mít velikost takovou, jako je celkové zrychlení, pokud se plošina neotáčí.
\end{enumerate}

Každý z~těchto problémů jsem vyřešil otáčením celkového vektoru zrychlení.
Proti prvnímu jsem zakročil otáčením vektoru v~rovině \textit{xz}. Tím pádem jsem mohl \uv{přebytečné} tečné zrychlení v~klidu přesunout k~tíhovému vektoru.
\begin{enumerate}
\item Nejprve si všechny vektory promítnu do roviny \textit{xz}.
\item Pak si pomocí skalárního součinu zjistím úhly svírané promítnutými vektory se zápornou osou \textit{z}.
      Ty odpovídají úhlu, o~který je potřeba vektory otočit, aby měly nulové tečné složky.
\item Dále najdu všechny body, ve kterých je úhlová rychlost menší než \SI{10}{\degree\per\second}.
      Nad touto množinou bodů poté spočítám průměrný úhel, o~který by bylo potřeba vektory otočit.
\item Nakonec všechny vektory otočím okolo osy \textit{y} o~vypočítaný průměrný úhel. Tím jsem částečně odstranil vliv gravitačního působení na tečnou osu.
\end{enumerate}

Druhý problém jsem řešil otáčením v~rovině \textit{xy}. Mohl jsem tak eliminovat přebytečné zrychlení na tečné ose způsobené vlivem dostředivého zrychlení.
\begin{enumerate}
\item Opět jsem si promítnul všechny vektory do roviny \textit{xy} a pak jsem spočítal jejich odchylku od osy \textit{y}.
\item Dál jsem určil množinu bodů, kde se rameno otáčelo rychlostí nejvýše o~\SI{50}{\degree\per\sec} pomalejší, než byla jeho celková nejvyšší úhlová rychlost.
      Zde jsem mohl předpokládat, že senzor bude vykonávat rovnoměrný pohyb po kružnici, tudíž by tečné zrychlení mělo být nulové.
\item Na předchozí množině jsem určil průměrný úhel, o~který jsem pak otočil všechny vektory okolo osy \textit{z}.
      Tak jsem vyrušil vliv dostředivého působení na tečnou osu.
\end{enumerate}

Posledním úkolem bylo stabilizovat tíhovou složku na nějaké stálé hodnotě.
Nyní jsem vektory otáčel v~rovině \textit{yz}, takže se tíhové zrychlení korigovalo vzhledem k~dostředivému zrychlení.
\begin{enumerate}
\item Ze všech míst, kde byla velikost úhlové rychlosti menší než \SI{10}{\degree\per\second}, jsem spočítal průměrnou velikost celkového zrychlení.
      To jsem musel provést, protože průměrné zrychlení v~klidu nebylo vždy rovno \SI{9.81}{\meter\per\square\second}.
\item Všechny vektory jsem si promítnul do roviny \textit{yz}, hodnoty tečného zrychlení se tedy už nezmění.
\item V~rovině \textit{yz} jsem spočítal odchylku všech vektorů od osy \textit{z}.
      Zároveň jsem si spočítal odchylku, při níž by tíhová složka vektoru byla rovna klidovému zrychlení.
\item Vektory jsem pak otočil okolo osy \textit{x} o~rozdíl předchozích dvou odchylek.
      Tím pádem má tíhová složka vektoru, pokud je celková délka dostačující, velikost jako zrychlení vklidu.
\end{enumerate}

Výsledný graf tečné složky zrychlení pak opravdu má špičky v~místech, kde se mění rychlost otáčení.
V~místech, kde rychlost zůstávala neměnná, se zrychlení pohybuje kolem nuly.

\subsubsection{Skládání měření}
Abych snížil úroveň šumu a zkombinoval pět roztočení ramene do jednoho kompaktního časového grafu, data mezi jednotlivými pokusy jsem zprůměroval.
Rozpoznání a zarovnání pohybů zajišťují přechodné body -- překročení úhlové rychlosti \SI{90}{\degree\per\second} (v~obou směrech).
Do okna pohybu se ještě počítá \SI{1000}{\milli\second} před přechodem a \SI{500}{\milli\second} po přechodu.
