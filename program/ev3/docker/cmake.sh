#!/bin/bash
mkdir -p /home/compiler/program/build
cd /home/compiler/program/build
cmake .. -DCMAKE_BUILD_TYPE=$BUILD_TYPE -DCMAKE_EXPORT_COMPILE_COMMANDS=ON -DCMAKE_TOOLCHAIN_FILE=/home/compiler/ev3-toolchain.cmake
