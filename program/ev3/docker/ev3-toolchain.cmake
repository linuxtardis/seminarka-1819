# this one is important
SET(CMAKE_SYSTEM_NAME Linux)
#this one not so much
SET(CMAKE_SYSTEM_VERSION 4.14.0)

# specify the cross compiler
SET(CMAKE_C_COMPILER arm-linux-gnueabi-gcc)

SET(CMAKE_CXX_COMPILER arm-linux-gnueabi-g++)

SET(CMAKE_CXX_FLAGS "-march=armv5t -mtune=arm926ej-s" CACHE STRING "CXX compiler flags" FORCE)
