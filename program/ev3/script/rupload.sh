#!/bin/bash
DIR="$( cd "$( dirname $( readlink -f "${BASH_SOURCE[0]}" ) )/../prg" >/dev/null 2>&1 && pwd )"

scp "$DIR/build/robogympl" "ev3dev:/home/robot/a_program"
ssh ev3dev sudo setcap cap_sys_nice+ep /home/robot/a_program
