//
// Created by kuba on 13.4.19.
//

#define _GNU_SOURCE

#include <btnative.h>

#include <unistd.h>
#include <signal.h>

#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>

#include <bluetooth/bluetooth.h>
#include <bluetooth/rfcomm.h>

int do_close(int fd) {
    return close(fd);
}

int do_socket() {
    return socket(AF_BLUETOOTH, SOCK_STREAM | SOCK_NONBLOCK, BTPROTO_RFCOMM);
}

int do_bind_any(int fd, int channel) {
    struct sockaddr_rc addr = {};
    addr.rc_family = AF_BLUETOOTH;
    addr.rc_channel = channel;
    memset(&addr.rc_bdaddr, 0, sizeof(addr.rc_bdaddr));

    return bind(fd, (struct sockaddr *) &addr, sizeof(addr));
}

int do_listen(int fd, int queue) {
    return listen(fd, queue);
}

int do_accept(int fd) {
    struct sockaddr_rc addr = {};
    socklen_t alen = sizeof(addr);
    return accept4(fd, (struct sockaddr *) &addr, &alen, SOCK_NONBLOCK);
}

int do_recv(int fd, char *data, size_t len) {
    return recv(fd, data, len, 0);
}

int do_write(int fd, const char *data, size_t len) {
    return write(fd, data, len);
}

int do_sigint(void (*handler)(int)) {
    struct sigaction sa = {NULL};
    sa.sa_handler = handler;
    if (sigfillset(&sa.sa_mask) == -1) {
        return -1;
    }
    if (sigaction(SIGINT, &sa, NULL) == -1) {
        return -1;
    }
    return 0;
}

void do_raise(int signal) {
    raise(signal);
}
