//
// Created by kuba on 25.3.19.
//

#ifndef ROBOGYMPL_RAWREG_HPP
#define ROBOGYMPL_RAWREG_HPP

#include <robofat.h>
#include "scheduler.hpp"

namespace rg {
    class program;

    class rawreg : public task {
    public:
        explicit rawreg(program &prg);

        void update() override;

        void setRequest(float req);
        void start();
        void stop();

        static int regulateMotor(float request, float current, float elapsed, robofat::pid &pid);

        robofat::pid pid;
    private:
        float timeCalc();

        timestamp m_last = {};
        float m_req;
        bool m_active;
    };
}


#endif //ROBOGYMPL_RAWREG_HPP
