//
// Created by kuba on 18.3.19.
//

#ifndef PRG_GLOBAL_HPP
#define PRG_GLOBAL_HPP

#include <memory>
#include <device_store.hpp>
#include <scheduler.hpp>
#include <Connection.h>
#include <settings.h>
#include <device_store.hpp>
#include "rawreg.hpp"
#include "controller.hpp"
#include "btcomm.hpp"
#include "load_measure.hpp"

namespace rg {
    struct program {
        explicit program(const std::string &conf_path);

        ~program();

        robofat::settings cfg;
        scheduler sched;
        idle_task idle;
        ev3udev::udev_handle udev;
        ev3udev::Registry reg;
        ev3udev::Connection conn;
        device_store dev;
        rawreg mreg;
        controller ctrl;
        btcomm comm;
        load_measure load;
    };
}

#endif //PRG_GLOBAL_HPP
