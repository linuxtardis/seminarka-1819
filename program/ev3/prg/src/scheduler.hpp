//
// Created by kuba on 18.3.19.
//

#ifndef PRG_SCHEDULER_HPP
#define PRG_SCHEDULER_HPP

#include <memory>
#include <list>
#include <atomic>
#include <robofat.h>

namespace rg {
    class program;

    class task {
    public:
        explicit task(const std::string &name, program &prg);

        virtual ~task() = default;

        const std::string name() const;

        virtual void update() = 0;

        virtual int iterationPeriod() {
            return 1;
        }

        bool isEnabled() const {
            return m_enabled;
        }

        void enable() {
            m_enabled = true;
        }

        void disable() {
            m_enabled = false;
        }

    protected:
        program *m_prg;

    private:
        bool m_enabled = true;
        const std::string m_name;
    };

    class idle_task : public task {
    public:
        explicit idle_task(program &prg, long msec);

        void update() override;

        void skip();

        static void thread_sleep(timespec &req);

        static void thread_sleep(long msec);

        int getTarget() const {
            return (int) m_period;
        }

    private:
        void update_time(timespec &out);

        void do_sleep();

        void update_start();

        void update_end();

        void calculate_loop_time();

        bool m_hasStart = false;
        bool m_skipNext = false;
        long m_period;
        timespec m_start = {};
        timespec m_end = {};
        timespec m_diff = {};
    };

    class scheduler {
    public:
        scheduler();

        void append(task &ptr) {
            m_tasks.push_front(&ptr);
        }

        void remove(task &ptr) {
            m_tasks.remove(&ptr);
        }

        void enterLoop();

        void setLooping(bool rly) {
            m_loop->store(rly);
        }

    private:
        void kernelSetup();

        std::list<task *> m_tasks;
        std::shared_ptr<std::atomic<bool>> m_loop;
    };
}

#endif //PRG_SCHEDULER_HPP
