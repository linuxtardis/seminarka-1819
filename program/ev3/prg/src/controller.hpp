//
// Created by kuba on 13.4.19.
//

#ifndef ROBOGYMPL_CONTROLLER_HPP
#define ROBOGYMPL_CONTROLLER_HPP

#include "scheduler.hpp"

namespace rg {
    enum class ctrl_state {
        RAMP_UP,
        STEADY,
        RAMP_DOWN,
    };

    class controller : public task {
    public:
        controller(program &prg);

        void update() override;

        void run(float speed, float acceleration);
    private:
        float m_target;
        float m_accel;
        float m_orig;
        timestamp m_start;
        ctrl_state m_state;
    };
}


#endif //ROBOGYMPL_CONTROLLER_HPP
