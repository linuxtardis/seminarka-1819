//
// Created by kuba on 18.3.19.
//

#include <Common.h>
#include "scheduler.hpp"
#include "exceptions.hpp"
#include "btnative.h"
#include <cstring>

const std::string rg::task::name() const {
    return m_name;
}

rg::task::task(const std::string &name, rg::program &prg)
        : m_name(name), m_prg(&prg) {}

void rg::scheduler::enterLoop() {
    kernelSetup();

    int n = 0;
    while (m_loop->load()) {
        for (task *tsk : m_tasks) {
            if (tsk->isEnabled() && n % tsk->iterationPeriod() == 0) {
                tsk->update();
            }
        }
        n++;
        if (n == 1000)
            n = 0;
    }
}

std::shared_ptr<std::atomic<bool>> global_stop;

static void global_sigint(int) {
    global_stop->store(false);
}

void rg::scheduler::kernelSetup() {
    sched_param attrs = {};
    attrs.sched_priority = sched_get_priority_min(SCHED_FIFO);

    if (::sched_setscheduler(0, SCHED_RR, &attrs) == -1) {
        ev3udev::log_errno("sched_setattr", "<scheduler>");
    }

    global_stop = m_loop;

    do_sigint(&global_sigint);
}

rg::scheduler::scheduler() : m_loop(std::make_shared<std::atomic<bool>>(true)) {

}

rg::idle_task::idle_task(program &prg, long msec)
        : task("idle", prg), m_period(msec) {}


void rg::idle_task::calculate_loop_time() {
    if ((m_end.tv_nsec - m_start.tv_nsec) < 0) {
        m_diff.tv_sec = m_end.tv_sec - m_start.tv_sec - 1;
        m_diff.tv_nsec = m_end.tv_nsec - m_start.tv_nsec + 1000000000;
    } else {
        m_diff.tv_sec = m_end.tv_sec - m_start.tv_sec;
        m_diff.tv_nsec = m_end.tv_nsec - m_start.tv_nsec;
    }
}


void rg::idle_task::skip() {
    m_skipNext = true;
}

void rg::idle_task::update_time(timespec &out) {
    if (clock_gettime(CLOCK_MONOTONIC, &out) == -1) {
        ev3udev::raise_errno("clock_gettime", this->name());
    }
}

void rg::idle_task::update_start() {
    update_time(m_start);
    m_skipNext = false;
    m_hasStart = true;
}

void rg::idle_task::update_end() {
    update_time(m_end);
}

void rg::idle_task::update() {
    if (!m_skipNext && m_hasStart) {
        update_end();
        do_sleep();
    }
    update_start();
}

void rg::idle_task::do_sleep() {
    calculate_loop_time();

    if (m_diff.tv_sec > 0) {
        return;
    }
    timespec req = {
            .tv_sec = 0,
            .tv_nsec = m_period * 1000000L - m_diff.tv_nsec
    };

    if (req.tv_nsec > 0) {
        thread_sleep(req);
    }
}

void rg::idle_task::thread_sleep(timespec &req) {
    timespec rem = {};

    while (nanosleep(&req, &rem) == -1) {
        if (errno == EINTR) {
            req = rem;
            continue;
        } else {
            ev3udev::log_errno("nanosleep", "idle task");
            return;
        }
    }
}

void rg::idle_task::thread_sleep(long msec) {
    timespec req = {
            .tv_sec = 0,
            .tv_nsec = 1000000L * msec
    };
    thread_sleep(req);
}

