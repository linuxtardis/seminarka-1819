//
// Created by kuba on 14.4.19.
//

#include "load_measure.hpp"
#include "program.hpp"

void rg::load_measure::update() {
    float speed = (m_prg->dev.a.getCurrentSpeed() + m_prg->dev.b.getCurrentSpeed()) / 2.0f;
    m_csv << m_power << "," << speed << std::endl;

    if (m_rising) {
        if (m_power == 100) {
            m_rising = false;
        } else {
            m_power++;
        }
    } else {
        if (m_power == 0) {
            m_prg->sched.setLooping(false);
        } else {
            m_power--;
        }
    }
    m_prg->dev.a.setDutyCycleSP(m_power);
    m_prg->dev.b.setDutyCycleSP(m_power);
}

rg::load_measure::load_measure(rg::program &prg)
        : task("load msr", prg),
          m_csv("/home/robot/load.csv") {
}

int rg::load_measure::iterationPeriod() {
    return 100;
}
