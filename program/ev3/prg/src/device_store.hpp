//
// Created by kuba on 19.3.19.
//

#ifndef ROBOGYMPL_DEVICE_STORE_HPP
#define ROBOGYMPL_DEVICE_STORE_HPP

#include <motor.hpp>
#include <Registry.hpp>
#include <settings.h>

namespace rg {
    class program;

    struct device_store {
        explicit device_store(program &prg);

        motor a;
        motor b;
    };
}


#endif //ROBOGYMPL_DEVICE_STORE_HPP
