//
// Created by kuba on 25.3.19.
//

#include "rawreg.hpp"
#include "program.hpp"

rg::rawreg::rawreg(rg::program &prg)
        : task("raw motor regulator", prg),
          m_req(0.0f), m_active(false),
          pid(prg.cfg.get_float("reg kp"),
                prg.cfg.get_float("reg ki"),
                prg.cfg.get_float("reg kd"),
                prg.cfg.get_float("reg imult"),
                prg.cfg.get_float("reg imax"),
                prg.cfg.get_float("reg dfilt now"),
                prg.cfg.get_float("reg ff k"),
                prg.cfg.get_float("reg ff q")) {
    tsGet(m_last);
}

int rg::rawreg::regulateMotor(float request, float current, float elapsed, robofat::pid &pid) {
    float err = request - current;

    bool overload = false;
    float pwr = pid.process(request, err, elapsed);
    if (pwr > 100.0f) {
        pwr = 100.0f;
        overload = true;
    }
    if (pwr < -100.0f) {
        pwr = -100.0f;
        overload = true;
    }
    pid.set_i_freeze(overload);
    return ftoi(pwr);
}

void rg::rawreg::update() {
    float elapsed = timeCalc();
    if (!m_active) {
        return;
    }

    float avgSpeed = (m_prg->dev.a.getCurrentSpeed() + m_prg->dev.b.getCurrentSpeed()) / 2.0f;
    int pwr = regulateMotor(m_req, avgSpeed, elapsed, pid);

    m_prg->dev.a.setDutyCycleSP(pwr);
    m_prg->dev.b.setDutyCycleSP(pwr);
}

void rg::rawreg::start() {
    if (!m_active) {
        setRequest(0.0f);
        m_prg->dev.a.setDutyCycleSP(0);
        m_prg->dev.b.setDutyCycleSP(0);
        m_prg->dev.a.setStopAction(stop_action::brake);
        m_prg->dev.b.setStopAction(stop_action::brake);
        m_prg->dev.a.doThe(motor_action::run_direct);
        m_prg->dev.b.doThe(motor_action::run_direct);
        pid.get_state().reset();
        m_active = true;
    }
}

void rg::rawreg::stop() {
    if (m_active) {
        m_prg->dev.a.setStopAction(stop_action::hold);
        m_prg->dev.b.setStopAction(stop_action::hold);
        m_prg->dev.a.doThe(motor_action::stop);
        m_prg->dev.b.doThe(motor_action::stop);
        m_active = false;
    }
}

void rg::rawreg::setRequest(float req) {
    m_req = req;
}

float rg::rawreg::timeCalc() {
    timestamp now = {};
    timestamp diff = {};
    tsGet(now);
    tsDiff(m_last, now, diff);
    m_last = now;
    return tsMsec(diff);
}