//
// Created by kuba on 13.4.19.
//

#include "btcomm.hpp"
#include "btnative.h"

#include <program.hpp>

#include <algorithm>

#define RFCOMM_CHANNEL 1
#define RFCOMM_QUEUE 2

rg::btcomm::btcomm(rg::program &prg)
        : task("bt comm", prg),
          m_recv(),
          m_buf(1024, 0) {
    this->open();
}

rg::btcomm::~btcomm() {
    if (m_fd != -1) {
        if (do_close(m_fd) == -1) {
            ev3udev::log_errno("close", "<bt conn>");
        }
        m_fd = -1;
    }
    if (m_sock != -1) {
        if (do_close(m_sock) == -1) {
            ev3udev::log_errno("close", "<rfcomm>");
        }
        m_sock = -1;
    }
}

void rg::btcomm::open() {
    if ((m_sock = do_socket()) < 0) {
        ev3udev::raise_errno("socket", "<rfcomm>");
    }
    if (do_bind_any(m_sock, RFCOMM_CHANNEL)) {
        ev3udev::raise_errno("bind", "<rfcomm>");
    }

    if (do_listen(m_sock, RFCOMM_QUEUE) < 0) {
        ev3udev::raise_errno("listen", "<rfcomm>");
    }
}


void rg::btcomm::update() {
    if (m_fd == -1) {
        int fd = do_accept(m_sock);
        if (fd < 0) {
            if (errno == EAGAIN || errno == EWOULDBLOCK) {
                return;
            } else {
                ev3udev::raise_errno("accept", "<rfcomm>");
            }
        } else {
            m_fd = fd;
            std::cout << "CONNECTED" << std::endl << std::flush;
        }
    }

    int bytes = do_recv(m_fd, m_buf.data(), m_buf.size());
    if (bytes == 0 || errno == ECONNRESET) {
        std::cout << "DISCONNECTED" << std::endl << std::flush;
        if (do_close(m_fd) < 0) {
            ev3udev::raise_errno("close", "<bt conn>");
        }
        m_fd = -1;
        return;
    }
    if (bytes < 0) {
        if (errno == EAGAIN || errno == EWOULDBLOCK) {
            return;
        } else {
            ev3udev::raise_errno("recv", "<bt conn>");
        }
    }
    m_recv.insert(m_recv.end(), m_buf.begin(), m_buf.begin() + bytes);
    std::vector<char>::iterator it;
    while ((it = std::find(m_recv.begin(), m_recv.end(), '\n')) != m_recv.end()) {
        std::string line(m_recv.begin(), it);
        processInput(line);
        m_recv.erase(m_recv.begin(), it + 1);
    }
}

void rg::btcomm::processInput(const std::string &line) {
    std::vector<std::string> tokenized;
    bool skippingWs = true;

    auto it = line.begin(), end = line.end(), start = line.begin();
    for (; it != end; ++it) {
        char ch = *it;
        if (skippingWs) {
            if (ch == ' ' || ch == '\t') {
                continue;
            } else {
                start = it;
                skippingWs = false;
            }
        } else {
            if (ch == ' ' || ch == '\t') {
                std::string token(start, it);
                tokenized.push_back(std::move(token));
                skippingWs = true;
            } else {
                continue;
            }
        }
    }
    if (!skippingWs) {
        std::string token(start, it);
        if (!token.empty()) {
            tokenized.push_back(std::move(token));
        }
    }

    processInput(tokenized);
}

void rg::btcomm::processInput(const std::vector<std::string> &tokens) {
    if (tokens.empty()) {
        return;
    }

    auto &pid = m_prg->mreg.pid.get_constants();

    const std::string &cmd = tokens[0];
    if (cmd == "get") {
        auto it = tokens.begin() + 1;
        auto end = tokens.end();
        for (; it != end; ++it) {
            std::string var = *it;
            std::transform(var.begin(), var.end(), var.begin(), ::tolower);

            if (var == "p") {
                reply("Kp", pid.kp);
            } else if (var == "i") {
                reply("Ki", pid.ki);
            } else if (var == "d") {
                reply("Kd", pid.kd);
            } else if (var == "ffk") {
                reply("FF k", pid.ff_k);
            } else if (var == "ffq") {
                reply("FF q", pid.ff_q);
            } else {
                reply("Unknown param: " + var + "\n");
            }
        }
    } else if (cmd == "set") {
        if (tokens.size() != 3) {
            reply("Expected 2 arguments\n");
            return;
        }
        const std::string &var = tokens[1];
        const std::string &value = tokens[2];
        try {
            if (var == "p") {
                pid.kp = std::stof(value);
            } else if (var == "i") {
                pid.ki = std::stof(value);
            } else if (var == "d") {
                pid.kd = std::stof(value);
            } else if (var == "ffk") {
                pid.ff_k = std::stof(value);
            } else if (var == "ffq") {
                pid.ff_q = std::stof(value);
            } else {
                reply("Unknown param: " + var + "\n");
                return;
            }
            m_prg->mreg.pid.get_state().reset();
            reply("OK\n");
        } catch (std::invalid_argument &arg) {
            std::string what(arg.what());
            reply("Invalid format: '" + what + "'\n");
        }
    } else if (cmd == "read") {
        auto it = tokens.begin() + 1;
        auto end = tokens.end();
        for (; it != end; ++it) {
            std::string var = *it;
            std::transform(var.begin(), var.end(), var.begin(), ::tolower);

            if (var == "speed") {
                float speed = (m_prg->dev.a.getCurrentSpeed() + m_prg->dev.b.getCurrentSpeed()) / 2.0f;
                reply("speed", speed);
            } else if (var == "duty") {
                float duty = (m_prg->dev.a.getDutyCycleSP() + m_prg->dev.b.getDutyCycleSP()) / 2.0f;
                reply("dury", duty);
            } else {
                reply("Unknown info: " + var + "\n");
            }
        }
    } else if (cmd == "run") {
        if (tokens.size() != 3) {
            reply("Expected 2 arguments\n");
            return;
        }
        const std::string &targetS = tokens[1];
        const std::string &accelS = tokens[2];
        try {
            float target = stof(targetS);
            float accel = stof(accelS);
            m_prg->ctrl.run(target, accel);
            reply("OK\n");
        } catch (std::invalid_argument &arg) {
            std::string what(arg.what());
            reply("Invalid format: '" + what + "'\n");
        }
    } else {
        reply("Unknown command: " + cmd + "\n");
    }
}

void rg::btcomm::reply(const std::string &line) {
    if (do_write(m_fd, line.data(), line.length()) < 0) {
        ev3udev::raise_errno("write", "<bt conn>");
    }
}

void rg::btcomm::reply(const std::string &var, float value) {
    std::ostringstream ostr;
    ostr << var;
    ostr << " = ";
    ostr << value;
    ostr << "\n";
    reply(ostr.str());
}
