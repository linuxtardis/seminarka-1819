#include <utility>

//
// Created by kuba on 18.3.19.
//

#include "program.hpp"

rg::program::program(const std::string &conf_path)
        : cfg(conf_path),
          sched(),
          idle(*this, (long) cfg.get_integer("loop msec")),
          udev(ev3udev::make_udev()),
          reg(udev),
          conn(*this, udev, reg),
          dev(*this),
          mreg(*this),
          ctrl(*this),
          comm(*this),
          load(*this) {
    sched.append(conn);
    sched.append(comm);
    sched.append(ctrl);
    sched.append(mreg);
    sched.append(load);
    sched.append(idle);
    mreg.start();
}

rg::program::~program() {
    dev.a.doThe(motor_action::reset);
    dev.b.doThe(motor_action::reset);
}
