//
// Created by kuba on 14.4.19.
//

#ifndef ROBOGYMPL_LOAD_MEASURE_HPP
#define ROBOGYMPL_LOAD_MEASURE_HPP

#include "scheduler.hpp"

namespace rg {
    class load_measure : public task {
    public:
        explicit load_measure(program &prg);

        void update() override;

    private:
        int iterationPeriod() override;

        bool m_finished = false;
        bool m_rising = true;
        int m_power = 0;
        std::ofstream m_csv;
    };
}


#endif //ROBOGYMPL_LOAD_MEASURE_HPP
