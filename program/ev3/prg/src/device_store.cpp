//
// Created by kuba on 19.3.19.
//

#include <program.hpp>


rg::device_ptr findPort(ev3udev::Registry &reg, const std::string &port) {
    if (auto &&dev = reg.findForAddress(port)) {
        return dev;
    } else {
        std::ostringstream ostr;
        ostr << "Cannot find one of the devices: @" << port;
        throw std::runtime_error(ostr.str());
    }
}


rg::device_store::device_store(program &prg)
        : a(findPort(prg.reg, settings::port_java2cpp("b"))),
          b(findPort(prg.reg, settings::port_java2cpp("c"))) {
    a.doThe(motor_action::reset);
    b.doThe(motor_action::reset);
}
