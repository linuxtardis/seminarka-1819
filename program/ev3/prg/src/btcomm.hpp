//
// Created by kuba on 13.4.19.
//

#ifndef ROBOGYMPL_BTCOMM_HPP
#define ROBOGYMPL_BTCOMM_HPP

#include "scheduler.hpp"
#include <vector>

namespace rg {
    class btcomm : public task {
    public:
        explicit btcomm(program &prg);

        ~btcomm() override;

        void update() override;

        void processInput(const std::string &line);

        void processInput(const std::vector<std::string> &tokens);

    private:
        void open();

        void reply(const std::string &line);

        void reply(const std::string &var, float value);

        int m_sock = -1;
        int m_fd = -1;

        std::vector<char> m_recv;
        std::vector<char> m_buf;
    };
}


#endif //ROBOGYMPL_BTCOMM_HPP
