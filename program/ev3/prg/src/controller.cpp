//
// Created by kuba on 13.4.19.
//

#include "controller.hpp"
#include <program.hpp>

void rg::controller::update() {
    timestamp now = {};
    timestamp diff = {};
    tsGet(now);
    tsDiff(m_start, now, diff);
    float time = tsMsec(diff) / 1000.0f;

    if (m_state == ctrl_state::RAMP_UP) {
        float next = m_orig + time * m_accel;
        if (next > m_target) {
            m_state = ctrl_state::STEADY;
            m_prg->mreg.setRequest(m_target);
        } else {
            m_prg->mreg.setRequest(next);
        }
    } else if (m_state == ctrl_state::RAMP_DOWN) {
        float next = m_orig - time * m_accel;
        if (next < m_target) {
            m_state = ctrl_state::STEADY;
            m_prg->mreg.setRequest(m_target);
        } else {
            m_prg->mreg.setRequest(next);
        }
    } else {
        m_prg->mreg.setRequest(m_target);
    }
}

rg::controller::controller(rg::program &prg)
        : task("controller", prg),
          m_target(0),
          m_accel(0),
          m_start(),
          m_state(ctrl_state::STEADY) {

}

void rg::controller::run(float speed, float acceleration) {
    if (speed > m_target) {
        m_state = ctrl_state::RAMP_UP;
    } else if (speed < m_target) {
        m_state = ctrl_state::RAMP_DOWN;
    } else {
        m_state = ctrl_state::STEADY;
    }
    m_orig = m_target;
    m_target = speed;
    m_accel = acceleration;
    tsGet(m_start);
}
