//
// Created by kuba on 13.4.19.
//

#ifndef ROBOGYMPL_BTNATIVE_H
#define ROBOGYMPL_BTNATIVE_H


#ifdef __cplusplus
#include <cstddef>
extern "C" {
#else
#include <stddef.h>
#endif

extern int do_close(int fd);
extern int do_socket();
extern int do_bind_any(int fd, int channel);
extern int do_listen(int fd, int queue);
extern int do_accept(int fd);
extern int do_recv(int fd, char *data, size_t len);
extern int do_write(int fd, const char *data, size_t len);
extern int do_sigint(void (*handler)(int));
extern void do_raise(int signal);

#ifdef __cplusplus
}
#endif

#endif //ROBOGYMPL_BTNATIVE_H
