#include <iostream>
#include "program.hpp"
#include "exceptions.hpp"

const char *default_config_path = "/home/robot/etc/rotor.conf";

#define MSR false

int main(int argc, char **argv) {
    std::set_terminate(&rg::exception_hook);
    std::string confpath = default_config_path;
    if (argc == 2) {
        confpath = argv[1];
    }

    std::cerr << "Initializing program..." << std::endl << std::flush;
    std::unique_ptr<rg::program> pProgram(new rg::program(confpath));

    if (MSR) {
        pProgram->comm.disable();
        pProgram->ctrl.disable();
        pProgram->mreg.disable();
    } else {
        pProgram->load.disable();
    }

    std::cerr << "Ready!" << std::endl << std::flush;
    pProgram->sched.enterLoop();
    std::cerr << "Exit! " << std::endl << std::flush;
}
