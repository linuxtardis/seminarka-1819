//
// Created by kuba on 18.3.19.
//

#ifndef PRG_REGISTRY_HPP
#define PRG_REGISTRY_HPP

#include <string>
#include <list>
#include "Device.h"

namespace ev3udev {
    class Registry {
    public:
        typedef std::list<std::shared_ptr<Device>> device_list;

        explicit Registry(udev_handle udev);

        const device_list &getDevices() const;

        void deviceAdded(const std::string &syspath);

        void deviceChanged(const std::string &syspath);

        void deviceRemoved(const std::string &syspath);

        std::shared_ptr<Device> findForDriver(const std::string &driver_name);

        std::shared_ptr<Device> findForAddress(const std::string &address);

        std::shared_ptr<Device> findForSyspath(const std::string &syspath);

    private:
        device_list devices;
        udev_handle udev;
    };
}


#endif //PRG_REGISTRY_HPP
