#ifndef UDEVCONNECTION_INTERNAL_H
#define UDEVCONNECTION_INTERNAL_H

#include <libudev.h>
#include <atomic>
#include <vector>
#include <memory>

#include "scheduler.hpp"
#include "Common.h"

namespace ev3udev {
    class Registry;

    class Connection : public rg::task {
    public:
        Connection(rg::program &prg, udev_handle udevInstance, Registry &registry);

        Connection(rg::program &prg, udev_handle udevInstance, Registry &registry,
                   std::vector<std::string> match_subsystems);

        void enumerate();

        void update() override;

        int iterationPeriod() override {
            return 10;
        }

    private:
        std::vector<std::string> subsystems;
        udev_handle udev;
        udevmon_handle mon;
        Registry *registry;
    };

}

#endif // UDEVCONNECTION_INTERNAL_H
