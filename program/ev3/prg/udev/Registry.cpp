//
// Created by kuba on 18.3.19.
//

#include <utility>
#include "Registry.hpp"

void ev3udev::Registry::deviceAdded(const std::string &syspath) {
    std::cerr << "- device added: " << syspath << std::endl << std::flush;
    devices.emplace_back(std::make_shared<Device>(udev, syspath));
}

void ev3udev::Registry::deviceChanged(const std::string &syspath) {
    std::cerr << "- device changed: " << syspath << std::endl << std::flush;
    if (auto dev = findForSyspath(syspath)) {
        dev->handleUdevChange();
    }
}

void ev3udev::Registry::deviceRemoved(const std::string &syspath) {
    std::cerr << "- device removed: " << syspath << std::endl << std::flush;
    auto it = devices.begin();
    auto end = devices.end();
    while (it != end) {
        if ((*it)->get_sys_path() == syspath) {
            devices.erase(it++);
        } else {
            ++it;
        }
    }
}

ev3udev::Registry::Registry(ev3udev::udev_handle udev) : udev(std::move(udev)) {}

const ev3udev::Registry::device_list &ev3udev::Registry::getDevices() const {
    return devices;
}

std::shared_ptr<ev3udev::Device> ev3udev::Registry::findForDriver(const std::string &driver_name) {
    auto it = devices.begin();
    auto end = devices.end();
    for (; it != end; ++it) {
        if ((*it)->get_lego_driver() == driver_name) {
            return *it;
        }
    }
    return nullptr;
}

std::shared_ptr<ev3udev::Device> ev3udev::Registry::findForAddress(const std::string &address) {
    auto it = devices.begin();
    auto end = devices.end();
    for (; it != end; ++it) {
        if ((*it)->get_lego_address() == address) {
            return *it;
        }
    }
    return nullptr;
}

std::shared_ptr<ev3udev::Device> ev3udev::Registry::findForSyspath(const std::string &syspath) {
    auto it = devices.begin();
    auto end = devices.end();
    for (; it != end; ++it) {
        if ((*it)->get_sys_path() == syspath) {
            return *it;
        }
    }
    return nullptr;
}
