#include "Device.h"

#include <dirent.h>
#include <cstring>


////////////
// DEVICE //
////////////


ev3udev::Device::Device(udev_handle udev, const std::string &syspath)
        : syspath(syspath), udev(udev), dev(make_udevdev_from_syspath(udev, syspath)), is_present(true) {
    enumerateAttrs();
}

ev3udev::Device::~Device() {
    for (auto &&pair : attrs) {
        pair.second->handleRemove();
    }
    attrs.clear();
}


std::string ev3udev::Device::get_action() {
    return udev_device_get_action(dev.get());
}

std::string ev3udev::Device::get_sys_path() {
    return syspath;
}

std::string ev3udev::Device::get_sys_name() {
    return udev_device_get_sysname(dev.get());
}

std::string ev3udev::Device::get_subsystem() {
    return udev_device_get_subsystem(dev.get());
}

std::string ev3udev::Device::get_devtype() {
    return udev_device_get_devtype(dev.get());
}

std::string ev3udev::Device::get_lego_driver() {
    return udev_device_get_property_value(dev.get(), "LEGO_DRIVER_NAME");
}

std::string ev3udev::Device::get_lego_address() {
    return udev_device_get_property_value(dev.get(), "LEGO_ADDRESS");
}

bool ev3udev::Device::hasAttr(const std::string &name) {
    return attrs.find(name) != attrs.end();
}

std::shared_ptr<ev3udev::Attribute> ev3udev::Device::attr(const std::string &name) {
    return attrs.at(name);
}

void ev3udev::Device::enumerateAttrs() {
    for (auto &&pair : attrs) {
        pair.second->handleRemove();
    }
    attrs.clear();

    auto &&path = get_sys_path();
    iterateDir(path, path);
}

void ev3udev::Device::iterateDir(const std::string &base, const std::string &path) {
    DIR *handle = opendir(path.c_str());
    if (handle == nullptr) {
        raise_errno("opendir", path);
    }

    errno = 0;
    for (struct dirent *dir = readdir(handle);
         dir != nullptr;
         dir = readdir(handle)) {

        if (dir->d_type == DT_REG) {
            appendAttr(base, path + "/" + dir->d_name);
        }

        if (dir->d_type == DT_DIR) {
            if (strcmp(dir->d_name, "hold_pid") == 0 || strcmp(dir->d_name, "speed_pid") == 0) {
                iterateDir(base, path + "/" + dir->d_name);
            }
        }
    }
    if (errno != 0) {
        raise_errno("readdir", path);
    }
}

void ev3udev::Device::appendAttr(const std::string &base, const std::string &path) {
    unsigned long relStart = base.length();
    unsigned long relLimit = path.length();
    while (relStart < relLimit && path[relStart] == '/')
        relStart++;

    std::string relPath = path.substr(relStart);
    attrs.emplace(std::make_pair(relPath, std::make_shared<Attribute>(path)));
}

void ev3udev::Device::handleUdevChange() {
    udevdev_handle newDev = make_udevdev_from_syspath(udev, get_sys_path());
    dev = newDev;
    //enumerateAttrs();
}

bool ev3udev::Device::present() {
    return is_present;
}

void ev3udev::Device::handleUdevRemove() {
    is_present = false;
}
