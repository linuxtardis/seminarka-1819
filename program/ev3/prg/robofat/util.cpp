/*
 * RoboFAT, a small C++ robotic library for ev3dev
 * Copyright (C) 2016  Jakub Vaněk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Common function implementation.
 */

#include <cmath>
#include "util.h"

using std::string;

void robofat::utility::rtrim(string &str) {
    size_t end = str.find_last_not_of(" \t\r\n");
    if (end + 1 == str.length())
        return;
    if (end == string::npos)
        str.erase();
    else
        str.erase(end + 1);
}

void robofat::utility::ltrim(string &str) {
    size_t start = str.find_first_not_of(" \t\r\n");
    if (start == 0)
        return;
    if (start == string::npos)
        str.erase();
    else
        str.erase(0, start);
}

void robofat::utility::trim(string &str) {
    ltrim(str);
    rtrim(str);
}


long robofat::utility::calc_delta_us(instant &last) {
    instant now = std::chrono::steady_clock::now();
    duration diff = now - last;
    last = now;

    return duration_cast<std::chrono::microseconds>(diff).count();
}

long robofat::utility::calc_delta_us(timespec &last) {
    timespec now = {};
    clock_gettime(CLOCK_MONOTONIC, &now);
    // Modified copy of example in GLIBC documentation (http://www.gnu.org/software/libc/manual/html_node/Elapsed-Time.html)
    /* Perform the carry for the later subtraction by updating y. */
    if (now.tv_nsec < last.tv_nsec) {
        long sec = (last.tv_nsec - now.tv_nsec) / 1000000000 + 1;
        last.tv_nsec -= 1000000000 * sec;
        last.tv_sec += sec;
    }
    if (now.tv_nsec - last.tv_nsec > 1000000000) {
        long sec = (now.tv_nsec - last.tv_nsec) / 1000000000;
        last.tv_nsec += 1000000000 * sec;
        last.tv_sec -= sec;
    }

    long result = now.tv_nsec - last.tv_nsec + (now.tv_sec - last.tv_sec) * 1000000000;
    last = now;
    return result / 1000;
}

