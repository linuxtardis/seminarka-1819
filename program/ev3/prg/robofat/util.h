/*
 * RoboFAT, a small C++ robotic library for ev3dev
 * Copyright (C) 2016  Jakub Vaněk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Common functions.
 */

#ifndef ROBOFAT_UTIL_H
#define ROBOFAT_UTIL_H

#include <string>
#include <chrono>
#include <type_traits>
#include <cmath>
#include <ctime>
#include <memory>
#include <Common.h>

using std::chrono::steady_clock;
using std::chrono::duration_cast;

typedef std::chrono::duration<long, std::ratio<1, 1000000000>> duration;
typedef std::chrono::time_point<steady_clock, duration> instant;

namespace robofat {
    /**
     * \brief Common utility functions
     *
     * @author Jakub Vaněk <vanek.jakub4@seznam.cz>
     */
    namespace utility {
        /**
         * \brief Left-trim the string.
         *
         * @param str   Read-write string.
         */
        extern void ltrim(std::string &str);

        /**
         * \brief Right-trim the string.
         *
         * @param str   Read-write string.
         */
        extern void rtrim(std::string &str);

        /**
         * \brief Trim the string.
         *
         * @param str   Read-write string.
         */
        extern void trim(std::string &str);

        /**
         * \brief Calculate microsecond delta time from last instant using C++ standard library.
         * Gets last instant, calculates delta time from current instant and sets last instant to current instant
         *
         * @param last  Last instant
         * @return      Delta time in microseconds.
         */
        extern long calc_delta_us(instant &last);

        /**
         * \brief Calculate microsecond delta time from last instant using C/POSIX standard library.
         * Gets last instant, calculates delta time from current instant and sets last instant to current instant
         *
         * @param last  Last instant
         * @return      Delta time in microseconds.
         */
        extern long calc_delta_us(timespec &last);

        /**
         * \brief Get inversion sign from inversion bool
         *
         * @param invert   If the output should be inverted (i.e. negative)
         * @return         {@code 1} if {@code invert == false}, {@code -1} if {@code invert == true}
         */
        inline int inversion_sign(const bool invert) {
#ifdef MATH_OVER_BRANCH_SIGN
            return invert * -2 + 1;
#else
            return invert ? -1 : 1;
#endif
        }

        /**
         * \brief Reverse function to inversion_sign
         * @param arg   1 or -1
         * @return      {@code true} if {@code arg == -1}, false otherwise
         */
        inline bool inverted(const int arg) {
            return arg == -1;
        }

        /**
         * \brief Signum function of the specified value
         *
         * @param val   Argument to signum
         * @return      -1 if negative, 0 if 0, 1 if positive
         */
        template<typename number>
        inline number signum(number val) {
            return (0 < val) - (val < 0);
        }

        /**
         * \brief Clip the value to specified bounds
         *
         * @param value   Value to limit
         * @param min     Lower limit
         * @param max     Upper limit
         * @return        Clipped value
         */
        template<typename number>
        inline number limit(number value, number min, number max) {
#ifdef MATH_OVER_BRANCH_LIMIT
            int sign, diff;

            diff = value - min;
            sign = (diff >> 31) & 0x1;
            value = value - sign * diff;

            diff = value - max;
            sign = (diff >> 31) & 0x1;
            value = max + sign * diff;
#else
            if (value < min)
                value = min;
            if (value > max)
                value = max;
#endif
            return value;
        }

        template<typename number>
        inline bool is_bounded(number value, number min, number max) {
            return min < value && value < max;
        }

        /**
         * \brief Convert float to integer.
         * Rounds the float to a whole number and then casts it to int.
         *
         * @param arg   float to convert
         * @return      converted integer
         */
        inline int ftoi(const float arg) {
            return (int) std::round(arg);
        }

        /**
         * \brief Float PI
         */
        constexpr float PI_F = static_cast<float>(M_PI);
        /**
         * \brief Double PI
         */
        constexpr double PI = static_cast<double>(M_PI);

        template<typename T, typename... Args>
        std::unique_ptr<T> make_unique(Args&& ...args) {
            return std::unique_ptr<T>(new T(std::forward<Args>(args)...));
        }

        typedef timespec timestamp;

        inline void tsGet(timestamp &now) {
            if (clock_gettime(CLOCK_MONOTONIC, &now) < 0) {
                ev3udev::raise_errno("clock_gettime", "tsGet");
            }
        }

        inline void tsDiff(timestamp &start, timestamp &end, timestamp &diff) {
            if ((end.tv_nsec - start.tv_nsec) < 0) {
                diff.tv_sec = end.tv_sec - start.tv_sec - 1;
                diff.tv_nsec = end.tv_nsec - start.tv_nsec + 1000000000;
            } else {
                diff.tv_sec = end.tv_sec - start.tv_sec;
                diff.tv_nsec = end.tv_nsec - start.tv_nsec;
            }
        }

        inline float tsMsec(timestamp &diff) {
            return diff.tv_sec * 1000.0f + diff.tv_nsec / 1000000.0f;
        }
    }
}

#endif //ROBOFAT_UTIL_H
