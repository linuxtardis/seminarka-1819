/*
 * RoboFAT, a small C++ robotic library for ev3dev
 * Copyright (C) 2016  Jakub Vaněk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * PID regulator.
 */

#ifndef ROBOFAT_PID_H
#define ROBOFAT_PID_H

#include <cmath>
#include "pid_param.h"

namespace robofat {
    /**
     * \brief PID regulator
     * PID stands for Proportional, Integral and Derivative
     *
     * @author Jakub Vaněk <vanek.jakub4@seznam.cz>
     */
    class pid {
    public:
        /**
         * \brief Construct new PID instance
         *
         * @param kp   Proportional constant (Kp)
         * @param ki   Integral constant (Ki)
         * @param kd   Derivative constant (Kd)
         * @param filter_actual   Present weight for derivative filter
         * @param filter_hist     History weight for derivative filter
         */
        pid(float kp, float ki, float kd, float iForget, float iMax, float filter_actual, float ffK, float ffQ);

        /**
         * \brief Construct new PID instance
         *
         * @param consts   PID constants
         * @param filter   Derivative filter parameters
         */
        pid(pid_consts<float> &consts, pid_filter<float> &filter);

        /**
         * \brief Process new error value
         *
         * @param error   New error value
         * @param dt      Time from last processing
         * @return        New correction
         */
        float process(float req, float error, float dt);

        /**
         * \brief Get PID constants
         *
         * @return   PID constants
         */
        pid_consts<float> &get_constants();

        /**
         * \brief Set PID constants
         *
         * @param consts   PID constants
         */
        void set_constants(pid_consts<float> &consts);

        /**
         * \brief Get PID state
         *
         * @return   PID state
         */
        pid_state<float> &get_state();

        /**
         * \brief Set PID state
         *
         * @param state   PID state
         */
        void set_state(pid_state<float> &state);

        /**
         * \brief Get derivative filter parameters
         *
         * @return   Filter parameters
         */
        pid_filter<float> &get_filter();

        /**
         * \brief Set derivative filter parameters
         *
         * @param filter   Filter parameters
         */
        void set_filter(pid_filter<float> &filter);

        void set_i_freeze(bool rly) {
            freezeI = rly;
        }
    private:
        /**
         * \brief Average filter a derivative value.
         *
         * @param val   Derivative
         * @return      Filtered derivative
         */
        float filter(float val);

        bool freezeI = true;
        pid_consts<float> constants;    ///< PID constants
        pid_filter<float> filter_param; ///< Derivative filter parameters
        pid_state<float> state;         ///< PID state
    };
}

#endif //ROBOFAT_PID_H
