//
// Created by kuba on 18.3.19.
//

#include "motor.hpp"

rg::motor::motor(rg::device_ptr dev)
        : dev(dev),
          a_address(dev->attr("address")),
          a_command(dev->attr("command")),
          a_driver_name(dev->attr("driver_name")),
          a_duty_cycle(dev->attr("duty_cycle")),
          a_duty_cycle_sp(dev->attr("duty_cycle_sp")),
          a_hold_kp(dev->attr("hold_pid/Kp")),
          a_hold_ki(dev->attr("hold_pid/Ki")),
          a_hold_kd(dev->attr("hold_pid/Kd")),
          a_max_speed(dev->attr("max_speed")),
          a_polarity(dev->attr("polarity")),
          a_position(dev->attr("position")),
          a_position_sp(dev->attr("position_sp")),
          a_ramp_up_sp(dev->attr("ramp_up_sp")),
          a_ramp_down_sp(dev->attr("ramp_down_sp")),
          a_speed(dev->attr("speed")),
          a_speed_kp(dev->attr("speed_pid/Kp")),
          a_speed_ki(dev->attr("speed_pid/Ki")),
          a_speed_kd(dev->attr("speed_pid/Kd")),
          a_speed_sp(dev->attr("speed_sp")),
          a_state(dev->attr("state")),
          a_stop_action(dev->attr("stop_action")),
          a_time_sp(dev->attr("time_sp")) {

}

std::string rg::motor::getAddress() {
    return a_address->readString();
}

rg::motor_type rg::motor::getType() {
    std::string driver = a_driver_name->readString();
    if (driver == large_driver) {
        return motor_type::large;
    } else if (driver == medium_driver) {
        return motor_type::medium;
    } else {
        return motor_type::unknown;
    }
}

robofat::pid_consts<int> rg::motor::getHoldPID() {
    return robofat::pid_consts<int>(
            a_hold_kp->readInt(),
            a_hold_ki->readInt(),
            a_hold_kd->readInt(),
            0,
            0,
            0,
            0
    );
}

void rg::motor::setHoldPID(const robofat::pid_consts<int> &values) {
    a_hold_kp->writeInt(values.kp);
    a_hold_ki->writeInt(values.ki);
    a_hold_kd->writeInt(values.kd);
}

robofat::pid_consts<int> rg::motor::getSpeedPID() {
    return robofat::pid_consts<int>(
            a_speed_kp->readInt(),
            a_speed_ki->readInt(),
            a_speed_kd->readInt(),
            0,
            0,
            0,
            0
    );
}

void rg::motor::setSpeedPID(const robofat::pid_consts<int> &values) {
    a_speed_kp->writeInt(values.kp);
    a_speed_ki->writeInt(values.ki);
    a_speed_kd->writeInt(values.kd);
}

rg::stop_action rg::motor::getStopAction() {
    std::string action = a_stop_action->readString();
    if (action == "coast") {
        return stop_action::coast;
    } else if (action == "brake") {
        return stop_action::brake;
    } else if (action == "hold") {
        return stop_action::hold;
    } else {
        throw std::runtime_error("Unknown stop action: " + action);
    }
}

void rg::motor::setStopAction(rg::stop_action action) {
    std::string value;
    switch (action) {
        case stop_action::coast:
            value = "coast";
            break;
        case stop_action::brake:
            value = "brake";
            break;
        case stop_action::hold:
            value = "hold";
            break;
    }
    a_stop_action->writeString(value);
}

rg::motor_state rg::motor::getState() {
    auto &&list = a_state->readStringList();
    motor_state mask = motor_state::motor_none;

    for (auto &&element : list) {
        if (element.empty()) {
            continue;
        }
        if (element == "running") {
            mask = (motor_state) (mask | motor_running);
        } else if (element == "ramping") {
            mask = (motor_state) (mask | motor_ramping);
        } else if (element == "holding") {
            mask = (motor_state) (mask | motor_holding);
        } else if (element == "overloaded") {
            mask = (motor_state) (mask | motor_overloaded);
        } else if (element == "stalled") {
            mask = (motor_state) (mask | motor_stalled);
        } else {
            throw std::runtime_error("Unknown motor state: " + element);
        }
    }

    return mask;
}

int rg::motor::getCurrentDutyCycle() {
    return a_duty_cycle->readInt();
}

int rg::motor::getCurrentSpeed() {
    return a_speed->readInt();
}

int rg::motor::getCurrentPosition() {
    return a_position->readInt();
}

int rg::motor::getMaximumSpeed() {
    return a_max_speed->readInt();
}

bool rg::motor::isNormalPolarity() {
    return a_polarity->readString() == "normal";
}

void rg::motor::setPolarity(bool normal) {
    a_polarity->writeString(normal ? "normal" : "inversed");
}

int rg::motor::getTimeSP() {
    return a_time_sp->readInt();
}

void rg::motor::setTimeSP(int value) {
    a_time_sp->writeInt(value);
}

int rg::motor::getSpeedSP() {
    return a_speed_sp->readInt();
}

void rg::motor::setSpeedSP(int value) {
    a_speed_sp->writeInt(value);
}

int rg::motor::getPositionSP() {
    return a_position_sp->readInt();
}

void rg::motor::setPositionSP(int value) {
    a_position_sp->writeInt(value);
}

int rg::motor::getRampUpSP() {
    return a_ramp_up_sp->readInt();
}

void rg::motor::setRampUpSP(int value) {
    a_ramp_up_sp->writeInt(value);
}

int rg::motor::getRampDownSP() {
    return a_ramp_down_sp->readInt();
}

void rg::motor::setRampDownSP(int value) {
    a_ramp_down_sp->writeInt(value);
}

int rg::motor::getDutyCycleSP() {
    return a_duty_cycle_sp->readInt();
}

void rg::motor::setDutyCycleSP(int value) {
    a_duty_cycle_sp->writeInt(value);
}

void rg::motor::doThe(rg::motor_action action) {
    std::string value;
    switch (action) {
        case motor_action::run_forever:
            value = "run-forever";
            break;
        case motor_action::run_to_abs_pos:
            value = "run-to-abs-pos";
            break;
        case motor_action::run_to_rel_pos:
            value = "run-to-rel-pos";
            break;
        case motor_action::run_timed:
            value = "run-timed";
            break;
        case motor_action::run_direct:
            value = "run-direct";
            break;
        case motor_action::stop:
            value = "stop";
            break;
        case motor_action::reset:
            value = "reset";
            break;
    }
    a_command->writeString(value);
}
