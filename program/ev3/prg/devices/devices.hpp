//
// Created by kuba on 18.3.19.
//

#ifndef PRG_DEVICES_HPP
#define PRG_DEVICES_HPP

#include <memory>
#include <cstring>
#include <Registry.hpp>
#include "Attribute.h"
#include "Device.h"

namespace rg {
    typedef std::shared_ptr<ev3udev::Attribute> attribute_ptr;
    typedef std::shared_ptr<ev3udev::Device> device_ptr;

    template<typename T>
    T readBinaryInteger(const std::tuple<const char *, int> &tuple) {
        T value;
        if (std::get<1>(tuple) < sizeof(T)) {
            throw std::runtime_error("not enough data");
        }
        std::memcpy(&value, std::get<0>(tuple), sizeof(T));
        return value;
    }
}

#endif //PRG_DEVICES_HPP
