#include "I2Cdev.h"
#include "MPU6050.h"

#define INTERRUPT_PIN  8
#define POWER_PIN      6
#define LED_PIN       13
#define BT_ENABLE_PIN  4
#define BT_STATE_PIN   5

//#define MPU_RATE_DIV  49
#define MPU_RATE_DIV  9
#define MPU_DLPF      MPU6050_DLPF_BW_20
// axis to take as rate of rotation
#define GYRO_AXIS      0 // x -> 0, y -> 1, z -> 2
// axis to take as rate of acceleration
#define ACCEL_AXIS_1   2 // x -> 0, y -> 1, z -> 2
// axis to take as rate of acceleration
#define ACCEL_AXIS_2   1 // x -> 0, y -> 1, z -> 2

MPU6050 mpu;

volatile bool mpu_irq = false;
unsigned long time0 = 0;
ISR(PCINT0_vect) {
  mpu_irq = true;
}


inline void setupPins() {
  // mpu interrupt pin
  pinMode(     INTERRUPT_PIN, INPUT );
  digitalWrite(INTERRUPT_PIN, LOW   );

  // mpu vcc pin
  pinMode(         POWER_PIN, OUTPUT);
  digitalWrite(    POWER_PIN, LOW  );

  // signal led
  pinMode(           LED_PIN, OUTPUT);
  digitalWrite(      LED_PIN, LOW   );

  // bt AT mode
  pinMode(     BT_ENABLE_PIN, OUTPUT);
  digitalWrite(BT_ENABLE_PIN, LOW   );

  // bt connection signalling
  pinMode(      BT_STATE_PIN, INPUT );
  digitalWrite( BT_STATE_PIN, LOW   );

  // mpu interrupt pin handling
  PCICR  |= _BV(PCIE0);
  PCMSK0 |= _BV(PCINT4);
}

inline void setupSerial() {
  Serial.begin(115200);
  Serial1.begin(115200);
}

inline void waitForBT() {
  Serial.println(F("BT wait..."));
  delay(1000);
  while(digitalRead(BT_STATE_PIN) == LOW) {
    delay(1);
  }
  Serial.println(F("BT OK"));
  time0 = millis();
}

inline void setupI2C() {
  Serial.print(F("I2C init..."));
  Fastwire::setup(400, true);
  digitalWrite(POWER_PIN, HIGH);
  delay(10);
}

inline void setupMPU_Connect() {
  Serial.print(F("Init MPU... "));
  mpu.initialize();
  Serial.println(mpu.testConnection() ? F("OK") : F("KO"));
}

inline void setupMPU_Basic() {
  mpu.setRate(MPU_RATE_DIV);
  mpu.setDLPFMode(MPU_DLPF);
  mpu.setFullScaleAccelRange(MPU6050_ACCEL_FS_8);
  mpu.setFullScaleGyroRange(MPU6050_GYRO_FS_1000);
}

inline void setupMPU_Bias() {
  Serial.println(F("Bias cfg"));
  mpu.setXGyroOffset( +81);
  mpu.setYGyroOffset(+172);
  mpu.setZGyroOffset( +19);
  mpu.setXAccelOffset(-3277);
  mpu.setYAccelOffset(-1145);
  mpu.setZAccelOffset( +990);
}

inline void setupMPU_IRQ() {
  Serial.println(F("IRQ cfg"));
  mpu.setInterruptMode(false);
  mpu.setInterruptDrive(true);
  mpu.setInterruptLatch(true);
  mpu.setInterruptLatchClear(false);
  mpu.setIntEnabled(true);
}


inline void setupMPU_FIFO() {
  Serial.println(F("FIFO cfg"));
  mpu.setFIFOEnabled(true);
  mpu.setXGyroFIFOEnabled(true);
  mpu.setYGyroFIFOEnabled(true);
  mpu.setZGyroFIFOEnabled(true);
  mpu.setAccelFIFOEnabled(true);
  mpu.resetFIFO();
}


void setup() {
  setupSerial();
  setupPins();
  waitForBT();
  setupI2C();
  setupMPU_Connect();
  setupMPU_Basic();
  setupMPU_Bias();
  setupMPU_IRQ();
  setupMPU_FIFO();
  Serial.println("READY");
}

static void toggleLED() {
  static bool ledOn = false;
  ledOn = !ledOn;
  digitalWrite(LED_PIN, ledOn ? HIGH : LOW);
}

uint8_t ingress[12];
uint8_t outgress[20];
short gyro[3];
short accel[3];

static void motion6(short *gyro, short *accel, uint8_t *buffer) {
    accel[0] = (((int16_t)buffer[0]) << 8) | buffer[1];
    accel[1] = (((int16_t)buffer[2]) << 8) | buffer[3];
    accel[2] = (((int16_t)buffer[4]) << 8) | buffer[5];
    gyro[0] = (((int16_t)buffer[6]) << 8) | buffer[7];
    gyro[1] = (((int16_t)buffer[8]) << 8) | buffer[9];
    gyro[2] = (((int16_t)buffer[10]) << 8) | buffer[11];
}

static void transmit_fifo_binary() {
  while (mpu.getFIFOCount() >= 12) {
    toggleLED();

    // read
    unsigned long t = millis() - time0;
    mpu.getFIFOBytes(ingress, 12);

    // translate
    outgress[0] = 0xAA;
    outgress[1] = 0xAA;
    outgress[2] = (t >> 0) & 0xFF;
    outgress[3] = (t >> 8) & 0xFF;
    outgress[4] = (t >> 16) & 0xFF;
    outgress[5] = (t >> 24) & 0xFF;
    outgress[6] = ingress[1];
    outgress[7] = ingress[0];
    outgress[8] = ingress[3];
    outgress[9] = ingress[2];
    outgress[10] = ingress[5];
    outgress[11] = ingress[4];
    outgress[12] = ingress[7];
    outgress[13] = ingress[6];
    outgress[14] = ingress[9];
    outgress[15] = ingress[8];
    outgress[16] = ingress[11];
    outgress[17] = ingress[10];
    outgress[18] = 0xAA;
    outgress[19] = 0xAA;

    // write
    Serial1.write(outgress, sizeof(outgress));
  }
}

static void transmit_fifo_text() {
  while (mpu.getFIFOCount() > 12) {
    toggleLED();

    // read
    unsigned long t = millis();
    mpu.getFIFOBytes(ingress, 12);
    motion6(gyro, accel, ingress);

    Serial1.print(t);
    Serial1.print(",");
    Serial1.print(gyro[0] / 131.0f, 2);
    Serial1.print(",");
    Serial1.print(gyro[1] / 131.0f, 2);
    Serial1.print(",");
    Serial1.print(gyro[2] / 131.0f, 2);
    Serial1.print(",");
    Serial1.print(accel[0] * 9.81f / 16384.0f, 4);
    Serial1.print(",");
    Serial1.print(accel[1] * 9.81f / 16384.0f, 4);
    Serial1.print(",");
    Serial1.print(accel[2] * 9.81f / 16384.0f, 4);
    Serial1.print("\r\n");
  }
}

static void transmit_fifo_text_simple() {
  while (mpu.getFIFOCount() >= 12) {
    toggleLED();

    // read
    unsigned long t = millis();
    mpu.getFIFOBytes(ingress, 12);
    motion6(gyro, accel, ingress);

    Serial1.print(t);
    Serial1.print(",");
    Serial1.print(gyro[0] / 32.8f, 2);
    Serial1.print(",");
    Serial1.print(gyro[1] / 32.8f, 2);
    Serial1.print(",");
    Serial1.print(gyro[2] / 32.8f, 2);
    Serial1.print(",");
    Serial1.print(accel[0] * 9.81f / 4096.0f, 4);
    Serial1.print(",");
    Serial1.print(accel[1] * 9.81f / 4096.0f, 4);
    Serial1.print(",");
    Serial1.print(accel[2] * 9.81f / 4096.0f, 4);
    Serial1.print("\n");
  }
}

void loop() {
  if (mpu_irq) {
    mpu_irq = false;

    uint8_t state = mpu.getIntStatus();

    if (state & _BV(MPU6050_INTERRUPT_FIFO_OFLOW_BIT)) {
      Serial.println(F("FIFO reset"));
      mpu.resetFIFO();
      return;
    }
    if (state & _BV(MPU6050_INTERRUPT_DATA_RDY_BIT)) {
      transmit_fifo_text_simple();
    }
  }
}
