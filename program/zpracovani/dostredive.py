#!/usr/bin/env python3

import os
import os.path
from graf_lib import *


def do_plot(inname, radius1, radius2, outname):
    data = GraphData(f"program/data2/{inname}.csv")

    print("[Creating raw plot]")
    combo = ComboPlot(data, radius1, radius2)
    combo.draw()
    combo.save(f"obrazky/grafy/{outname}.pdf")


class ComboPlot(BasePlot):
    def __init__(self, data, radius1, radius2):
        super().__init__(cols=2, rows=1)
        self.scatter = self.axes[0]
        self.time = self.axes[1]

        self.data = data
        self.radius1 = radius1
        self.radius2 = radius2

    def draw(self):
        print("[drawing scatter]")
        self._do_scatter()
        print("[drawing time]")
        self._do_time()
        self._place_legend(self.scatter)

    def _do_scatter(self):
        self.scatter.set_xlabel("$\\omega / \\si{\\degree\\per\\second}$")
        self.scatter.set_ylabel("$a_d / \\si{\\meter\\per\\square\\second}$")

        dataset = self.data.raw

        x_series = dataset['gz']
        y_series = dataset['ay']
        x_min = x_series.min()
        x_max = x_series.max()
        x_space = np.linspace(x_min, x_max, num=1000)

        self.scatter.scatter(x_series, y_series,
                             **self.scatterParams, c='red',
                             label='Naměřená data')

        if self.radius2 is not None:
            self.scatter.plot(x_space, (x_space / 180 * np.pi) ** 2 * self.radius2,
                              **self.plotParams, c='blue',
                              label='Předpověď ($\\SI{' + str(self.radius2) + '}{\\meter}$)')

        if self.radius1 is not None:
            self.scatter.plot(x_space, (x_space / 180 * np.pi) ** 2 * self.radius1,
                              **self.plotParams, c='green',
                              label='Předpověď ($\\SI{' + str(self.radius1) + '}{\\meter}$)')

    def _do_time(self):
        self.time.set_xlabel("$t / \\si{\\second}$")

        dataset = self.data.rAvg

        x_series = dataset['gz']
        y_series = dataset['ay']

        if self.radius2 is not None:
            self.time.plot((x_series / 180 * np.pi) ** 2 * self.radius2,
                           **self.plotParams, c='blue',
                           label='Předpověď ($\\SI{' + str(self.radius2) + '}{\\meter}$)')

        if self.radius1 is not None:
            self.time.plot((x_series / 180 * np.pi) ** 2 * self.radius1,
                           **self.plotParams, c='green',
                           label='Předpověď ($\\SI{' + str(self.radius1) + '}{\\meter}$)')

        self.time.plot(y_series,
                       **self.plotParams, c='red',
                       label='Naměřená data')


if __name__ == '__main__':
    do_plot('dlouhe', 0.230, 0.240, 'dostredive_dlouhe')
    do_plot('stredni2', 0.150, 0.144, 'dostredive_stredni')
    do_plot('kratke2',  0.080, 0.085, 'dostredive_kratke')
