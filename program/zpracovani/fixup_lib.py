#!/usr/bin/env python3

print("importing...")
import sys
import numpy as np
import quaternion
import pandas as pd
import matplotlib.pyplot as plt

STILL_THRESHOLD = 10
MOVING_CONST_THRESHOLD = 50
AVERAGE_THRESHOLD = 90
AVERAGE_PRE=1000
AVERAGE_POST=500

def load_csv(path):
    print("loading csv...")
    csv = pd.read_csv(path, header=None, names=['time','gx','gy','gz','ax','ay','az'], index_col=0)
    return csv

def middle_unit_vector(u, v):
    raw = np.add(u, v)
    unit = raw / np.linalg.norm(raw)
    return unit

def new_vector(x, y, z):
    return np.array([x,y,z], dtype=np.float)

def vectors_from_table(csv, nX, nY, nZ):
    print(" - getting vecs")
    return [new_vector(x,y,z) for x,y,z in zip(csv[nX], csv[nY], csv[nZ])]

def vector_length(vec):
    return np.linalg.norm(vec)

def vector_lengths(vecs):
    return [vector_length(vec) for vec in vecs]

def normalized_vector(vec):
    return vec / vector_length(vec)

def normalized_vectors(vecs):
    return [normalized_vector(vec) for vec in vecs]

def project_to_unit_vector(v_from, v_to):
    return np.dot(v_from, v_to) * v_to

def project_to_plane(v_from, v_to):
    subtract = project_to_unit_vector(v_from, v_to)
    return v_from - subtract

def proc_gyro(csv):
    """ Rotate angular velocity vectors such that they point only along the Z-axis.
    This locks the rotation to the XY-plane.
    """
    print("processing gyro...")

    vecs = vectors_from_table(csv, 'gx', 'gy', 'gz')
    muts = []
    for vec in vecs:
        length = vector_length(vec)
        if vec[2] < 0.0:
            length = -length
        muts.append(new_vector(0, 0, length))

    data = pd.DataFrame(muts, columns=['gx', 'gy', 'gz'])
    return data

def proc_gyro_noop(csv):
    """ Return gyro data, but do not rotate them.
    """
    print("processing gyro...")

    vecs = vectors_from_table(csv, 'gx', 'gy', 'gz')
    data = pd.DataFrame(vecs, columns=['gx', 'gy', 'gz'])
    return data

def proc_get_g(vecs, gyro):
    print("calculating g...")
    lengths = [vector_length(vec) for vec,gz in zip(vecs, gyro["gz"]) if np.abs(gz) < STILL_THRESHOLD]
    mean = np.mean(lengths)
    print(f" - from {len(lengths)} samples got g = {mean}")
    return mean

def proc_tangent_to_gravity(vecs, gyro):
    print(" - tangent to gravity")

    cond = gyro["gz"].abs() < STILL_THRESHOLD
    cond.index = range(0, len(vecs))

    # project to XZ-plane
    xz_projs = [project_to_plane(vec, y_axis) for vec in vecs]
    xz_norms = normalized_vectors(xz_projs)

    # get mean angle between projections and y-axis on still intervals
    rot_angs = [np.arccos(np.dot(z_axis*-1, vec)) for vec in xz_norms]
    rot_ang = pd.Series(rot_angs)[cond].mean()

    # get rotation quaternion and rotate all vectors
    rot_vec  = y_axis * rot_ang
    rot_quat = quaternion.from_rotation_vector(rot_vec)
    rot      = [quaternion.rotate_vectors(rot_quat,vec) for vec in vecs]
    return rot

def proc_gravity_to_centripetal(vecs, gyro, g):
    print(" - gravity to centripetal")

    global yz_projs, yz_lens, ratio_real, ratio_new, alpha_real, alpha_new, rot_angs, rot_vecs

    # project to YZ-plane
    yz_projs = [project_to_plane(vec, x_axis) for vec in vecs]
    yz_lens  = vector_lengths(yz_projs)

    ratio_real = [-v[2]/l for v,l in zip(yz_projs, yz_lens)]
    ratio_new  = [ g/l if np.abs(g/l)<=1.0 else 1.0 for l in yz_lens]

    alpha_real = [np.arccos(r) for r in ratio_real]
    alpha_new  = [np.arccos(r) for r in ratio_new]
    rot_angs   = [(n-r) for n,r in zip(alpha_new, alpha_real)]

    # works on bad data, doesn't work on OK data
    # rot_angs   = [np.abs(a) for a in rot_angs]

    # get rotation quaternions and rotate all vectors
    rot_vecs  = [x_axis * rot_ang for rot_ang in rot_angs]
    rot_quats = [quaternion.from_rotation_vector(rot_vec) for rot_vec in rot_vecs]
    rot       = [quaternion.rotate_vectors(quat,vec) for quat,vec in zip(rot_quats, vecs)]
    return rot

def proc_tangent_to_centripetal(vecs, gyro):
    """ Rotate all acceleration vectors such that when angular velocity is constant,
    the tangential component is zero.
    """
    print(" - tangent to centripetal")
    # TODO FIXME this is hardcoded
    # cond = pd.Series([x > 750 and x < 1000 for x in range(0, len(vecs))])
    # workaround:
    cond = gyro['gz'] > (gyro['gz'].max()-MOVING_CONST_THRESHOLD)
    cond.index = range(0, len(vecs))

    # project to XY-plane
    xy_projs = [project_to_plane(vec, z_axis) for vec in vecs]
    xy_norms = normalized_vectors(xy_projs)

    # get mean angle between projections and y-axis on still intervals
    rot_angs = [np.arccos(np.dot(y_axis, vec)) for vec in xy_norms]
    rot_ang = pd.Series(rot_angs)[cond].mean()
    #rot_ang = pd.Series(rot_angs)[cond].median()

    # get rotation quaternion and rotate all vectors
    rot_vec  = z_axis * rot_ang
    rot_quat = quaternion.from_rotation_vector(rot_vec)
    rot      = [quaternion.rotate_vectors(rot_quat,vec) for vec in vecs]
    return rot

def proc_accel(csv, gyro):
    """ Rotate acceleration vectors such that gravity is always 9.81 m*s^-2 and
    that the tangential acceleration when rotating at constant velocity is around zero.
    """
    print("processing accel...")

    global v1, v2, v3, v4, v5, v6, v7

    v1 = vectors_from_table(csv, 'ax', 'ay', 'az')
    g  = proc_get_g(v1, gyro)
    v2 = proc_tangent_to_gravity(v1, gyro)
    v3 = proc_tangent_to_centripetal(v2, gyro)
    v4 = proc_gravity_to_centripetal(v3, gyro, g)

    data = pd.DataFrame(v4, columns=['ax', 'ay', 'az'])
    return data

def proc_merge(csv, gyro, accel):
    """ Merge dataframes from gyro and accel fixups.
    """
    df = pd.merge(gyro, accel, left_index=True, right_index=True)
    df.index = csv.index
    return df

def proc_average_time(dat):
    print("- calculating avg dT")
    time = dat.index.to_series().diff().mean()/1000
    print(f"  - dT = {time}")
    return time

def proc_average(dat):
    """ Average together similar runs of data.
    """
    print("averaging...")
    idx  = dat.index.to_series()
    up   = idx[(dat["gz"].abs()<AVERAGE_THRESHOLD).astype(float).diff()==-1.0]
    down = idx[(dat["gz"].abs()<AVERAGE_THRESHOLD).astype(float).diff()==1.0]
    intervals = [(start,end) for start,end in zip(up,down)]

    avgT = proc_average_time(dat)

    bins=[]
    for interval in intervals:
        bin_ = dat.truncate(before=interval[0]-AVERAGE_PRE, after=interval[1]+AVERAGE_POST)
        bin_.index = range(0,len(bin_))
        bin_.index = bin_.index.to_series().multiply(avgT)
        bins.append(bin_)

    avg = bins[0]
    for bin_ in bins[1:]:
        avg.add(bin_)
    avg.divide(len(bins))
    return avg

x_axis = new_vector(1,0,0)
y_axis = new_vector(0,1,0)
z_axis = new_vector(0,0,1)

if __name__ == '__main__':
    np.seterr(all='ignore')

    if len(sys.argv) != 2:
        print(f"Fixup script launcher for IPython notebook")
        print(f"Usage: %run {sys.argv[0]} <data.csv>", file=sys.stderr)
        sys.exit(1)

    csv   = load_csv(sys.argv[1])
    gyro  = proc_gyro_noop(csv)
    accel = proc_accel(csv, gyro)
    data  = proc_merge(csv, gyro, accel)
    pAvg  = proc_average(data)
    rAvg  = proc_average(csv)
    print("done")
