#!/usr/bin/env python3

from fixup_lib import *
import matplotlib as mpl
import abc

SPINE_COLOR = 'black'


def latexify(fig_width=4, fig_height=None):
    """Set up matplotlib's RC params for LaTeX plotting.
    Call this before plotting a figure.

    Parameters
    ----------
    fig_width : float, optional, inches
    fig_height : float,  optional, inches
    """

    # code adapted from http://www.scipy.org/Cookbook/Matplotlib/LaTeX_Examples

    # Width and max height in inches for IEEE journals taken from
    # computer.org/cms/Computer.org/Journal%20templates/transactions_art_guide.pdf

    if fig_height is None:
        golden_mean = (np.sqrt(5) - 1.0) / 2.0  # Aesthetic ratio
        fig_height = fig_width * golden_mean  # height in inches

    params = {  # 'backend': 'pdf',
        'text.latex.preamble': [r'\usepackage{gensymb}',
                                r'\usepackage{lmodern}',
                                r'\usepackage[czech]{babel}',
                                r'\usepackage[T1]{fontenc}',
                                r'\usepackage{siunitx}',
                                r'\sisetup{inter-unit-product=\ensuremath{{\cdot}},output-decimal-marker = {,}}'],
        'text.latex.unicode': True,
        # 'axes.labelsize': 8, # fontsize for x and y labels (was 10)
        # 'axes.titlesize': 8,
        # 'font.size': 8, # was 10
        # 'legend.fontsize': 8, # was 10
        # 'xtick.labelsize': 8,
        # 'ytick.labelsize': 8,
        'text.usetex': True,
        'figure.figsize': [fig_width / 2.54, fig_height / 2.54],
        'font.family': 'serif'
    }

    mpl.rcParams.update(params)


class MemoizedProperty:
    def __init__(self, generator):
        self.cache = None
        self.generate = generator

    def __get__(self, obj, type=None):
        name = f'_cache_{self.generate.__name__}'
        if not hasattr(obj, name):
            setattr(obj, name, self.generate(obj))
        return getattr(obj, name)

    def __set__(self, obj, type=None):
        raise AttributeError("Cannot set MemoizedProperty")

    def __delete__(self, instance):
        raise AttributeError("Cannot delete MemoizedProperty")


def memoize(generator):
    return MemoizedProperty(generator)


class GraphData:
    def __init__(self, path):
        self.path = path

    @memoize
    def raw(self):
        return load_csv(self.path)

    @memoize
    def gyro(self):
        return proc_gyro_noop(self.raw)

    @memoize
    def accel(self):
        return proc_accel(self.raw, self.gyro)

    @memoize
    def proc(self):
        return proc_merge(self.raw, self.gyro, self.accel)

    @memoize
    def pAvg(self):
        return proc_average(self.proc)

    @memoize
    def rAvg(self):
        raw = proc_average(self.raw)
        raw.index = raw.index.to_series()
        return raw


class BasePlot(abc.ABC):
    def __init__(self, cols, rows):
        self.cols = cols
        self.rows = rows
        self.count = cols * rows
        self.scatterParams = {
            'marker': '.',
            's': 2,
        }
        self.plotParams = {
            'marker': None,
            'linestyle': '-',
            'linewidth': 1,
            'markersize': 2,
        }
        if cols == 2:
            latexify(fig_width=17.0, fig_height=7.0)
        else:
            latexify(fig_width=13.0, fig_height=7.0)

        self.figure, self.axes = plt.subplots(nrows=rows, ncols=cols, sharey=True)

        if self.count > 1:
            self.axis = self.axes[0]
            self.axes = self.axes
        else:
            self.axis = self.axes
            self.axes = [self.axes]

        for ax in self.axes:
            BasePlot._format_axis(ax)

    @staticmethod
    def _format_axis(ax):
        for spine in ['top', 'right']:
            ax.spines[spine].set_visible(False)

        for spine in ['left', 'bottom']:
            ax.spines[spine].set_color(SPINE_COLOR)
            ax.spines[spine].set_linewidth(0.5)

        ax.xaxis.set_ticks_position('bottom')
        ax.yaxis.set_ticks_position('left')

        for axis in [ax.xaxis, ax.yaxis]:
            axis.set_tick_params(direction='out', color=SPINE_COLOR)

        ax.grid(b=True, which='major', axis='both', color='gray', linestyle='-', visible=True, linewidth=0.5)
        ax.grid(b=True, which='minor', axis='both', color='gray', linestyle=':', visible=True, linewidth=0.25)
        ax.minorticks_on()

    @staticmethod
    def _place_legend(ax):
        leg = ax.legend(loc='best')
        leg.get_frame().set_alpha(0.5)

    def draw(self):
        pass

    def save(self, path):
        print("[saving]")
        self.figure.savefig(path, bbox_inches='tight')
