#!/usr/bin/env python3

from bluetooth import *
from time import sleep


class EV3:
    """
    Connect to the EV3.
    This opens a serial connection to the controlling program.
    """

    def __init__(self, remote_addr, local_addr, channel_no):
        self.remote = (remote_addr, channel_no)
        self.local = (local_addr, channel_no)

    def __enter__(self):
        self.fd = BluetoothSocket(RFCOMM)
        self.fd.bind(self.local)
        self.fd.connect(self.remote)
        return EV3Connection(self.fd)

    def __exit__(self, *args):
        self.fd.close()


class EV3Connection:
    """ EV3 connection wrapper. """

    def __init__(self, fd):
        self.fd = fd
        self.send(f"")  # newline only to terminate a possible old unterminated command
        self.set("p", 0.0)
        self.set("i", 0.0)
        self.set("d", 0.0)

    def go(self, speed, accel):
        """ Send a command for EV3 to gradually change rotational
        speed to `speed` deg/s with `accel` acceleration in deg/s^2 """
        self.send(f"run {speed} {accel}")

    def set(self, prop, value):
        self.send(f"set {prop} {value}")

    def send(self, command):
        """ Send a single command to the EV3."""
        self.fd.send(f"{command}\n")


class Experiment:
    def __init__(self, ev3):
        self.points = [(0, 360, 1.5), (360, 360, 3.0), (720, 720, 3.0), (0, 720, 1.5)]
        self.ev3 = ev3
        self.stop()

    def _single(self):
        for speed, accel, time in self.points:
            print(f" - goto {speed} deg/s by {accel} deg/s^2; wait {time} secs")
            self.ev3.go(speed, accel)
            sleep(time)

    def perform(self):
        """ Control loop. """
        for i in range(0, 5):
            print(f"Run {i + 1}:")
            self._single()

    def stop(self):
        self.ev3.go(0, 720)


def main():
    print("Initializing...")
    with EV3("00:17:EC:04:D2:01", "5C:F3:70:62:86:B8", 1) as ev3:
        exp = Experiment(ev3)

        input("Ready. Press enter to start.")
        exp.perform()
    print("Done.")


if __name__ == '__main__':
    main()
