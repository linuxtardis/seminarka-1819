#!/usr/bin/env python3

import pandas as pd
import numpy  as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import struct

with open("dlouhe.bin", "rb") as f:
    data = f.read()

lastI = 0

dfList=[]
rowList=[]
for i, v in enumerate(data):
    if i == len(data) - 3:
        break
    if v == 0xAA and data[i+1] == 0xAA and data[i+2] == 0xAA and data[i+3] == 0xAA:
        d = i - lastI
        if d == 19 and data[i+4] == 0xAA:
            d = i - lastI + 1
            lastI = i + 1
        else:
            lastI = i
        dict0 = {'idx': i, 'diff': d}
        dfList.append(dict0)
        if d == 20:
            if (len(data) - (lastI+4)) < 16:
                continue
            ts, ax, ay, az, gx, gy, gz = struct.unpack_from("<I6h", data, offset=lastI+4)
            ts = ts / 1000.0
            gx = gx / 32.8
            gy = gy / 32.8
            gz = gz / 32.8
            ax = ax * 9.81 / 4096.0
            ay = ay * 9.81 / 4096.0
            az = az * 9.81 / 4096.0
            dict1 = {'ts': ts, 'gx': gx, 'gy': gy, 'gz': gz, 'ax': ax, 'ay': ay, 'az': az}
            rowList.append(dict1)


df = pd.DataFrame(dfList, columns=['idx', 'diff'])
tbl = pd.DataFrame(rowList, columns=['ts', 'gx', 'gy', 'gz', 'ax', 'ay', 'az'])
